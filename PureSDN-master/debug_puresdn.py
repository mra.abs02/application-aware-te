#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from ryu.cmd import manager


def main():
    sys.argv.append('--ofp-tcp-listen-port')
    sys.argv.append('6653')
    # sys.argv.append('static')
    # sys.argv.append('network_awareness_pure')
    # sys.argv.append('network_awareness_mux')
    # sys.argv.append('topology_discovery')
    # sys.argv.append('bandwidth_monitor')
    # sys.argv.append('delay_monitor')
    sys.argv.append('PureSDN')
    # sys.argv.append('--verbose')
    sys.argv.append('--enable-debugger')
    sys.argv.append('--observe-links')
    # sys.argv.append('--k-paths=2')
    # sys.argv.append('--weight=bw')

    # sys.argv.append('--observe-hosts')
    manager.main()


if __name__ == '__main__':
    main()
