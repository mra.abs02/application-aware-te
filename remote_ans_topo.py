#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, Node
from mininet.node import CPULimitedHost
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Link, Intf

def ansTopo():
    NODE1_IP = '192.168.56.101'
    NODE2_IP = '192.168.56.102'
    CONTROLLER_IP = '192.168.56.1'

    net = Mininet(topo=None, build=False, host=CPULimitedHost, link=TCLink)

    net.addController('c0', controller=RemoteController, ip=CONTROLLER_IP, port=6653)  # )

    # add nodes, switches first...
    Hartford = net.addSwitch('s1', protocols='OpenFlow13')
    NewYork = net.addSwitch('s2', protocols='OpenFlow13')
    Chicago = net.addSwitch('s3', protocols='OpenFlow13')
    Cleveland = net.addSwitch('s4', protocols='OpenFlow13')
    Greensboro = net.addSwitch('s5', protocols='OpenFlow13')
    Atlanta = net.addSwitch('s6', protocols='OpenFlow13')
    dc = net.addSwitch('s7', protocols='OpenFlow13')
    Reston = net.addSwitch('s8', protocols='OpenFlow13')
    Dallas = net.addSwitch('s9', protocols='OpenFlow13')
    StLouis = net.addSwitch('s10', protocols='OpenFlow13')
    Seattle = net.addSwitch('s11', protocols='OpenFlow13')
    Denver = net.addSwitch('s12', protocols='OpenFlow13')
    SanFrancisco = net.addSwitch('s13', protocols='OpenFlow13')
    SanJose = net.addSwitch('s14', protocols='OpenFlow13')
    LosAngeles = net.addSwitch('s15', protocols='OpenFlow13')
    Albuquerque = net.addSwitch('s16', protocols='OpenFlow13')
    Hawaii = net.addSwitch('s17', protocols='OpenFlow13')
    Houston = net.addSwitch('s18', protocols='OpenFlow13')

    # ... and now hosts
    Hartford_host = net.addHost('h1')
    NewYork_host = net.addHost('h2')
    Chicago_host = net.addHost('h3')
    Cleveland_host = net.addHost('h4')
    Greensboro_host = net.addHost('h5')
    Atlanta_host = net.addHost('h6')
    dc_host = net.addHost('h7')
    Reston_host = net.addHost('h8')
    Dallas_host = net.addHost('h9')
    StLouis_host = net.addHost('h10')
    Seattle_host = net.addHost('h11')
    Denver_host = net.addHost('h12')
    SanFrancisco_host = net.addHost('h13')
    SanJose_host = net.addHost('h14')
    LosAngeles_host = net.addHost('h15')
    Albuquerque_host = net.addHost('h16')
    Hawaii_host = net.addHost('h17')
    Houston_host = net.addHost('h18')

    # add edges between switch and corresponding host
    net.addLink(Hartford, Hartford_host)
    net.addLink(NewYork, NewYork_host)
    net.addLink(Chicago, Chicago_host)
    net.addLink(Cleveland, Cleveland_host)
    net.addLink(Greensboro, Greensboro_host)
    net.addLink(Atlanta, Atlanta_host)
    net.addLink(dc, dc_host)
    net.addLink(Reston, Reston_host)
    net.addLink(Dallas, Dallas_host)
    net.addLink(StLouis, StLouis_host)
    net.addLink(Seattle, Seattle_host)
    net.addLink(Denver, Denver_host)
    net.addLink(SanFrancisco, SanFrancisco_host)
    net.addLink(SanJose, SanJose_host)
    net.addLink(LosAngeles, LosAngeles_host)
    net.addLink(Albuquerque, Albuquerque_host)
    net.addLink(Hawaii, Hawaii_host)
    net.addLink(Houston, Houston_host)

    # add edges between switches
    net.addLink(Hartford, NewYork, bw=10, delay='0.860766066795ms')
    net.addLink(Hartford, Cleveland, bw=10, delay='0.858556601658ms')
    net.addLink(NewYork, Cleveland, bw=10, delay='0.858617204215ms')
    net.addLink(NewYork, dc, bw=10, delay='0.605826192092ms')
    net.addLink(NewYork, Reston, bw=10, delay='0.649384477036ms')
    net.addLink(Chicago, Denver, bw=10, delay='1.35964770335ms')
    net.addLink(Chicago, StLouis, bw=10, delay='1.12827896944ms')
    net.addLink(Chicago, Cleveland, bw=10, delay='0.232315346482ms')
    net.addLink(Greensboro, Atlanta, bw=10, delay='1.34722977105ms')
    net.addLink(Greensboro, dc, bw=10, delay='1.53401911797ms')
    net.addLink(Atlanta, Houston, bw=10, delay='1.34344500256ms')
    net.addLink(dc, Reston, bw=10, delay='0.0702210950563ms')
    net.addLink(Reston, Dallas, bw=10, delay='0.100520786302ms')
    net.addLink(Reston, StLouis, bw=10, delay='0.205138815923ms')
    net.addLink(Dallas, StLouis, bw=10, delay='0.256389964194ms')
    net.addLink(Dallas, Houston, bw=10, delay='1.65690128332ms')
    net.addLink(Dallas, SanJose, bw=10, delay='0.975907242494ms')
    net.addLink(Seattle, Denver, bw=10, delay='1.11169346865ms')
    net.addLink(Seattle, SanFrancisco, bw=10, delay='1.54076081649ms')
    net.addLink(Denver, SanFrancisco, bw=10, delay='0.889017541903ms')
    net.addLink(SanFrancisco, SanJose, bw=10, delay='0.380977783485ms')
    net.addLink(SanFrancisco, LosAngeles, bw=10, delay='0.602936230151ms')
    net.addLink(LosAngeles, Albuquerque, bw=10, delay='0.784360426658ms')
    net.addLink(Albuquerque, Hawaii, bw=10, delay='0.844494798698ms')
    net.addLink(Albuquerque, Houston, bw=10, delay='0.577547683142ms')

    # Delete old tunnel if still exists
    Houston.cmd('ip tun del s1-gre1')
    # Create GRE tunnel
    Houston.cmd('ip li ad s1-gre1 type gretap local ' + NODE1_IP + ' remote ' + NODE2_IP + ' ttl 64')
    Houston.cmd('ip li se dev s1-gre1 up')
    Intf('s1-gre1', node=Houston)

    net.start()
    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    ansTopo()
