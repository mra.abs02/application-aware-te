# this is the sample network flow algorithm provided by networkx package.

import networkx as nx

G = nx.DiGraph()
G.add_node('1', demand=-3)
G.add_node('4', demand=3)
G.add_edge('1', '2', weight=1, capacity=7)
G.add_edge('1', '3', weight=1, capacity=3)
G.add_edge('2', '3', weight=1, capacity=5)
G.add_edge('2', '4', weight=1, capacity=4)
G.add_edge('3', '4', weight=1, capacity=4)

#flowDic = nx.min_cost_flow(G)
max_min = nx.max_flow_min_cost(G, '1', '4')
print(max_min)

