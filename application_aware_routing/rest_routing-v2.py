# TODO check if the flows and bandwidth are in kbps or mbps. line 272 in bw_mon has the for conversion.
# TODO if it could not solve the problem in a resonable time then I can create a graph from the k shortest paths for each flow. for this I need to import the host_location form the ryu app
# flow_speed with no priority, the speed is already converted.

link_to_port = {(1, 2): (1, 1), (3, 2): (2, 2), (1, 3): (2, 1), (4, 5): (2, 2), (3, 4): (3, 1), (3, 1): (1, 2), (5, 4): (2, 2), (2, 1): (1, 1), (2, 3): (2, 2), (4, 3): (1, 3), (2, 5): (3, 1), (5, 2): (1, 3)}
host_location = {(1, 3): ('10.0.0.1', '02:87:f0:32:85:d1'), (5, 3): ('10.0.0.5', 'c6:27:53:80:82:e2')}


import requests
from networkx.readwrite import json_graph
from collections import defaultdict
import ast
import numpy as np
import time
import networkx as nx
from docplex.mp.model import Model
import docplex.mp.linear


URL = "http://127.0.0.1:8080/no/mac/data"


class NetworkData(object):
	def __init__(self, *args, **kwargs):
		self.edges = set()
		self.flows = set()
		self.flow_demands = {}
		self.nodes = defaultdict(dict)
		self.non_splitable_flows = set()
		self.splitable_flows = set()
		self.graph_bw = nx.DiGraph()

	def display_net_data(self):
		print("\n----------- Network Flow Data ---------\n")
		print(
			"\tedges {0}\n\tflows: {1}\n\tflow_demand: {2}\n\t flow_nodes {3}\n\tnon_splitable_flows{4}\n\tsplitable_flows {5}").format(
			self.edges, self.flows,
			self.flow_demands, self.nodes, self.non_splitable_flows, self.splitable_flows)
		print("\n------------------------------------------")


net_data = NetworkData()
model = Model('MCFP')
path_nodes = defaultdict(list)
flow_endpoints = defaultdict(dict)
added_subpath = defaultdict(list) # to add the sub path to this dictionary for check up


def find_in_out_edges():
	"""
		Finds the in_edges and out_edges for each node in the network.
		TODO it is used for .....
	:return: None
	"""
	for e in net_data.edges:
		i = e[0]
		j = e[1]

		if i not in net_data.nodes or j not in net_data.nodes:
			print("[Error] The node of the edge %s doesn't exist." % e)
		else:
			# create a list out/in for node i
			net_data.nodes[i].setdefault('outEdges', [])
			net_data.nodes[i].setdefault('inEdges', [])

			# create a list out/in for node i
			net_data.nodes[j].setdefault('inEdges', [])
			net_data.nodes[j].setdefault('inEdges', [])

			# print("nodes_set_V[%s]: %s " %(i,nodes_set_V[i]))
			net_data.nodes[i]['outEdges'] += [e]
			net_data.nodes[j]['inEdges'] += [e]


def define_objective_function(x, y):
	"""
		Defines the objective function for the MIP model.
	:param x: continuous variables for splitable flows.
	:param y: binary variables for non-splitable flows.
	:return: None
	"""
	model.minimize(model.sum(
		(model.sum((net_data.graph_bw.get_edge_data(i, j).get('cost', 0) if net_data.graph_bw.get_edge_data(i, j) is not None else 0) * x[i, j][f] for f in net_data.splitable_flows)) +
		(model.sum((net_data.graph_bw.get_edge_data(i, j).get('cost', 0) if net_data.graph_bw.get_edge_data(i,j) is not None else 0) * y[i, j][nsf] * net_data.flow_demands[nsf] for nsf in net_data.non_splitable_flows)) for i, j in net_data.edges))

def link_capacity_constraints(x, y):
	"""
		Adds the link capacity constrains to the MIP model.
	:param x: Continuous variables for the splitable variables.
	:param y: Binary variables for the non-splitable variables.
	:return: None
	"""
	for i, j in net_data.edges:
		edge_data = net_data.graph_bw.get_edge_data(i, j)
		if edge_data is None:
			link_capacity = float('inf')
		else:
			link_capacity = edge_data['bandwidth']

		model.add_constraint(
			(((model.sum(x[i, j][f] for f in net_data.splitable_flows)) +
			  (model.sum(y[i, j][nsf] * net_data.flow_demands[nsf] for nsf in net_data.non_splitable_flows))
			  ) <= (link_capacity)), "link_constraint_(%s, %s)" % (i, j))

def flow_conservation_Splitable(x):
	"""
		Adds flow_conservation for splitable flows.
	:param: x is the splitable flows variables.
	:return: None
	"""
	for f in net_data.splitable_flows:
		source = f[0]
		target = f[1]

		for i, node_i_dic in net_data.nodes.iteritems():
			# set the flow balance, supply for each node
			# print("i ", i,"dic: ", node_i_dic)
			if i == source:
				traffic = float(net_data.flow_demands[f])
			elif i == target:
				traffic = float(-net_data.flow_demands[f])
			else:
				traffic = float(0)

			out_edges = node_i_dic.get('outEdges', [])
			in_edges = node_i_dic.get('inEdges', [])

			# for e in out_edges:
			# 	print("print e %s and %s f" %(e, f))
			# 	print("x:: ", x[e][f])
			# print("i %s and f %s"%(i, f))
			# sums = model.sum(x[e][f] for e in out_edges) - model.sum(x[e][f] for e in in_edges)
			# print sums
			model.add_constraint(
				(model.sum(x[e][f] for e in out_edges) - model.sum(x[e][f] for e in in_edges)) == traffic,
				"flow conservation for node %s, flow %s" % (i, f)
			)

def flow_conservation_non_splitable(y):
	"""
		Adds flow conservation constraints for non-splitable flows.
	:param y: Binary variable.
	:return: None
	"""
	for f in net_data.non_splitable_flows:
		source = f[0]
		target = f[1]

		for i, node_i_dic in net_data.nodes.iteritems():
			# set the flow balance, supply for each node
			# print("i ", i,"dic: ", node_i_dic)
			if i == source:
				traffic = 1
			elif i == target:
				traffic = -1
			else:
				traffic = 0

			out_edges = node_i_dic.get('outEdges', [])
			in_edges = node_i_dic.get('inEdges', [])

			# for e in out_edges:
			# 	print("print e %s and %s f" %(e, f))
			# 	print("x:: ", x[e][f])
			# print("i %s and f %s"%(i, f))
			# sums = model.sum(x[e][f] for e in out_edges) - model.sum(x[e][f] for e in in_edges)
			# print sums
			model.add_constraint(
				(model.sum(y[e][f] for e in out_edges) - model.sum(y[e][f] for e in in_edges)) == traffic,
				"Binary flow conservation for node %s, flow %s" % (i, f)
			)

def display_model_info():
	"""
		Print the information about the model.
	:return: None.
	"""
	print("------------ The objective function ------------- ")
	print(model.objective_expr)

	print("----------------- Model Info -----------------------")
	print(model.print_information())

	print("----------------- Constraints ----------------------")
	for c in model.iter_constraints():
		print("linear constraints: ", c)


def subpath_isdisjoint(p1, p2):
	if set(p1).isdisjoint(p2):
		return True


def find_dst_path(flow, curr_path, t_paths, src, dst):
	tot_path = t_paths[:]
	# print("total path(in find_dst_path): ", total_paths)
	if curr_path in tot_path:
		tot_path.remove(curr_path)
	path = []
	partial_paths = []

	for p in t_paths:
		tmp_path = []
		# print("<pre> total path(in find_dst_path): ", total_paths)
		if p not in added_subpath[
			flow]:  #### TODO I think this is not necessary. I just need to check if a path has been selected befor or not
			print("find_dst_path: curr_path: ", curr_path, "p: ", p, "t_paths: ", t_paths)
			if curr_path[-1] == p[0]:
				if subpath_isdisjoint(curr_path[:-1], p):
					if p[-1] == dst[0]:
						tmp_path = curr_path[:-1] + p
						path.append(tmp_path)  # it removes the duplicate nodes and add it to the path
						if p in tot_path:
							tot_path.remove(p)
						# added_subpath[flow].append(p) #TODO this variable is used to not over check the paths again and again
						# print("<post> total path(in find_dst_path): ", total_paths)
					else:
						tmp_path = curr_path[:-1] + p
						partial_path = find_dst_path(flow, tmp_path, t_paths, src, dst)
						path.append(partial_path)
	return path

def find_source_destination_edges(flow, value_dict):
	src_edge = None
	dst_edge = None
	# print("flow ",flow)
	for _, path in value_dict.items():
		for e in path:
			if flow[0] == e[0]:
				# print("source edge", e)
				src_edge = e
			elif flow[1] == e[1]:
				# print("destination edge", e)
				dst_edge = e

	return src_edge, dst_edge
def get_path_nodes(cplex_path):
	G = nx.DiGraph()

	# now find the nodes in the paths
	for flow in cplex_path:
		src, dst = find_source_destination_edges(flow, cplex_path[flow])
		if src and dst:
			# print("flow ", flow, src, dst)
			flow_endpoints[flow]['src'] = src
			flow_endpoints[flow]['dst'] = dst
			for amount, p in cplex_path[flow].items():
				# print("amount ", amount, "path",  p)
				path = p[:]
				if src in path:
					path.remove(src)
				if dst in path:
					path.remove(dst)

				# print(path)
				if path:
					G = nx.DiGraph()
					G.add_edges_from(path)
					# dfs_nods = list(nx.dfs_preorder_nodes(G, source=src[1]))
					# dfs_nods = list(nx.dfs_preorder_nodes(G)) # ### I believe because the paths/graph is directed and there is only on root so I don't need to specify the source node
					dfs_nods = list(nx.topological_sort(G))
					# print("dfs ", dfs_nods)
					path_nodes[flow].append(dfs_nods)
					del (G)
	return path_nodes

def get_complete_path(path_nodes):
	complete_paths = defaultdict(list)

	for flow, paths in path_nodes.items():
		# TODO To use added_subpaths variable  to avoid and not over check the paths again and again
		# TODO also check if two subpaths have any nodes in common, except the end nodes (otherwise they will cause loop). use not set(a).isdisjoint(b) check this link(https://stackoverflow.com/questions/3170055/test-if-lists-share-any-items-in-python)
		src_edge = flow_endpoints[flow]['src']
		dst_edge = flow_endpoints[flow]['dst']
		total_paths = paths[:]  # this will be passed to the find_dst_paths
		for p in paths:
			# print("----------")
			# exec("if dst[0] != int(p[-1]): print('yes')"),

			# print( "src %s, p[0] %s,dst %s, p[-1] %s,p %s " %(src_edge[1], p[0],dst_edge[0],p[-1],p))
			# print("++++++++++")
			# print("(src_edge %s, src_edge[1] %s, type(src_edge[1]) %s, p[0] %s,  type(p[0]) %s, dst_edge[0] %s, type(dst_edge[0]) %s, p[-1] %s, type(p[-1]) %s, p %s)" %(src_edge, src_edge[1], type(src_edge[1]),p[0], type(p[0]), dst_edge[0],type(dst_edge[0]), p[-1], type(p[-1]), p) )
			tmp_path = []
			print("<pre> total_path: ", total_paths)
			if src_edge[1] == p[0] and dst_edge[0] == p[-1]:

				tmp_path.append(src_edge[0])
				tmp_path.append(dst_edge[1])
				tmp_path = tmp_path[:1] + p + tmp_path[-1:]
				# print("the path for ", flow, "is ", p, "tmp", tmp_path)
				complete_paths[flow].append(tmp_path)
				# total_paths.remove(p)
				print("<post> total path: ", total_paths)

			elif src_edge[1] == p[0] and dst_edge[0] != p[-1]:
				tmp_path = find_dst_path(flow, p, paths, src_edge, dst_edge)
				print("in the main_for partial_path: ", tmp_path)
				complete_paths[flow].append(tmp_path)
			# find_the_path_that_joins this path to the destination node for key k
		# else if src[1] != p[0] and dst[0] == p[-1]:
		#     # fint the path that joins this path to the source for key k
	return complete_paths

def convert_to_path(path):
	path_nodes = get_path_nodes(path)
	print(path_nodes)
	path_nodes = {('10.0.0.1', '10.0.0.5'): [[1, 2, 5]], ('10.0.0.5', '10.0.0.1'): [[5, 2, 4], [4, 3, 1], [5, 4], [4, 2, 1], ]}
	complete_path = get_complete_path(path_nodes)
	print(complete_path)

def cplex_mode():
	"""
		Models the network for CPLEX.
	:return: None
	"""
	# XXX this for testing only.
	# net_data.splitable_flows.add(('10.0.0.5', '10.0.0.1'))
	net_data.splitable_flows.add(('10.0.0.1', '10.0.0.5'))
	net_data.non_splitable_flows.remove(('10.0.0.1', '10.0.0.5'))
	print("splitable flows %s" %net_data.splitable_flows)
	print("non-splitable flows %s" %net_data.non_splitable_flows)

	# Define the continuous variables
	x = {(i, j): {(s, t): model.continuous_var(name="x_(%s,%s)^f(%s,%s)" % (i, j, s, t)) for s, t in net_data.splitable_flows} for i, j in net_data.edges}

	# Define the binary variables
	y = {(i, j): {(s, t): model.binary_var(name="y_(%s,%s)^f(%s,%s)" % (i, j, s, t)) for s, t in net_data.non_splitable_flows} for i, j in net_data.edges}

	# Define the objective function
	define_objective_function(x, y)

	# add the link capacity constraints
	link_capacity_constraints(x, y)

	# add flow conservation constraints.
	flow_conservation_Splitable(x)
	flow_conservation_non_splitable(y)

	# display the model information
	display_model_info()

	# Solve the model
	solution = model.solve(log_output=True)
	print("----------------- solution ---------------")
	print(solution)
	print("--------------------------------------------")

	splitable_flows_paths = defaultdict(lambda: defaultdict(list))

	# get the splitable flows on each edge
	for edge in x.keys():
		# print(type(edge))
		for flow in x[edge].keys():
			# print(x[edge][flow])
	# 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key]._get_solution_value()))
	# 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key].get_name()))
	# 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key].solution_value))
			sol_value = x[edge][flow].solution_value
			if sol_value is 0:
				continue
			else:
				splitable_flows_paths[flow][sol_value].append(edge)
	print(splitable_flows_paths)
	convert_to_path(splitable_flows_paths)
			# print(model.get_var_by_name(str(x[k][key])))
	# print("%s, %s", net_data.graph_bw[1][2]['cost'])
	# print("%s, %s", net_data.graph_bw[2][5]['cost'])
	# print("x = %s", x)
	# print("x = %s", y)

	# d = {(1, 2): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 9},
	# 	 (3, 2): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (1, 3): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 4},
	# 	 (1, u'10.0.0.1'): {
	# 		 ('10.0.0.5', '10.0.0.1'): 2,
	# 		 ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (4, 5): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 4},
	# 	 (3, 4): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 4},
	# 	 (3, 1): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (u'10.0.0.5', 5): {
	# 		 ('10.0.0.5', '10.0.0.1'): 2,
	# 		 ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (5, 4): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (2, 1): {('10.0.0.5', '10.0.0.1'): 2,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (2, 3): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (5, u'10.0.0.5'): {
	# 		 ('10.0.0.5', '10.0.0.1'): 0,
	# 		 ('10.0.0.1', '10.0.0.5'): 9},
	# 	 (4, 3): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (u'10.0.0.1', 1): {
	# 		 ('10.0.0.5', '10.0.0.1'): 0,
	# 		 ('10.0.0.1', '10.0.0.5'): 9},
	# 	 (2, 5): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 5},
	# 	 (5, 2): {('10.0.0.5', '10.0.0.1'): 2,
	# 			  ('10.0.0.1', '10.0.0.5'): 0}}
	#
	# G1 = nx.from_dict_of_dicts(d)

def request_get(url):
	# try:
	# 	req_get = requests.get(url)
	#
	# 	if req_get.status_code != 200:
	# 		print("GET error: {}".format(req_get.status_code))
	# exit()
	# network = req_get.json()
	# except:
	# print("cannot connect to the server, sleeping...")

# example 1 with splitable flows and congestion in the network.

	network = {u'new_flows': {u"('10.0.0.1', '10.0.0.5')": {u'path': [1, 2, 5], u'type': u'S'}}, u'flow_speed_priority': {u'1': {u"(1, '10.0.0.1', '10.0.0.5')": [1200971.0], u"(1, '10.0.0.5', '10.0.0.1')": [23368.0]}, u'3': {}, u'2': {u"(1, '10.0.0.1', '10.0.0.5')": [1100991.0], u"(1, '10.0.0.5', '10.0.0.1')": [23401.0]}, u'5': {u"(1, '10.0.0.1', '10.0.0.5')": [1099510.0], u"(1, '10.0.0.5', '10.0.0.1')": [23401.0]}, u'4': {}}, u'graph': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}, {u'id': u'10.0.0.5'}, {u'id': u'10.0.0.1'}], u'links': [{u'source': 0, u'target': 0, u'weight': 0}, {u'source': 0, u'target': 1, u'weight': 1}, {u'source': 0, u'target': 2, u'weight': 1}, {u'source': 0, u'mac_add': u'9e:33:34:2a:9a:64', u'weight': 1, u'dpid_port': 3, u'target': 6}, {u'source': 1, u'target': 0, u'weight': 1}, {u'source': 1, u'target': 1, u'weight': 0}, {u'source': 1, u'target': 2, u'weight': 1}, {u'source': 1, u'target': 4, u'weight': 1}, {u'source': 2, u'target': 0, u'weight': 1}, {u'source': 2, u'target': 1, u'weight': 1}, {u'source': 2, u'target': 2, u'weight': 0}, {u'source': 2, u'target': 3, u'weight': 1}, {u'source': 3, u'target': 2, u'weight': 1}, {u'source': 3, u'target': 3, u'weight': 0}, {u'source': 3, u'target': 4, u'weight': 1}, {u'source': 4, u'mac_add': u'd2:8e:a4:c8:f4:94', u'weight': 1, u'dpid_port': 3, u'target': 5}, {u'source': 4, u'target': 1, u'weight': 1}, {u'source': 4, u'target': 3, u'weight': 1}, {u'source': 4, u'target': 4, u'weight': 0}, {u'source': 5, u'mac_add': u'd2:8e:a4:c8:f4:94', u'weight': 1, u'dpid_port': 3, u'target': 4}, {u'source': 6, u'mac_add': u'9e:33:34:2a:9a:64', u'weight': 1, u'dpid_port': 3, u'target': 0}], u'multigraph': False}, u'flow_speed': {u'1': {u"('10.0.0.1', '10.0.0.5')": [9607.768], u"('10.0.0.5', '10.0.0.1')": [186.944]}, u'3': {}, u'2': {u"('10.0.0.1', '10.0.0.5')": [8807.928], u"('10.0.0.5', '10.0.0.1')": [187.208]}, u'5': {u"('10.0.0.1', '10.0.0.5')": [8796.08], u"('10.0.0.5', '10.0.0.1')": [187.208]}, u'4': {}}, u'delay': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}], u'links': [{u'delay': 0, u'source': 0, u'target': 0, u'weight': 0}, {u'lldpDelay': 0.17779088020324707, u'delay': 0.1767369508743286, u'target': 1, u'weight': 1, u'source': 0}, {u'lldpDelay': 0.0030040740966796875, u'delay': 0.0013120174407958984, u'target': 2, u'weight': 1, u'source': 0}, {u'lldpDelay': 0.0029020309448242188, u'delay': 0.0018461942672729492, u'target': 0, u'weight': 1, u'source': 1}, {u'delay': 0, u'source': 1, u'target': 1, u'weight': 0}, {u'lldpDelay': 0.0025911331176757812, u'delay': 0.001723170280456543, u'target': 2, u'weight': 1, u'source': 1}, {u'lldpDelay': 0.004682064056396484, u'delay': 0.004734992980957031, u'target': 4, u'weight': 1, u'source': 1}, {u'lldpDelay': 0.0023500919342041016, u'delay': 0.0012731552124023438, u'target': 0, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.002621889114379883, u'delay': 0.0050460100173950195, u'target': 1, u'weight': 1, u'source': 2}, {u'delay': 0, u'source': 2, u'target': 2, u'weight': 0}, {u'lldpDelay': 0.0025818347930908203, u'delay': 0.001538395881652832, u'target': 3, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.002341032028198242, u'delay': 0.0013605356216430664, u'target': 2, u'weight': 1, u'source': 3}, {u'delay': 0, u'source': 3, u'target': 3, u'weight': 0}, {u'lldpDelay': 0.0031681060791015625, u'delay': 0.0014965534210205078, u'target': 4, u'weight': 1, u'source': 3}, {u'lldpDelay': 0.0023109912872314453, u'delay': 0.0013430118560791016, u'target': 1, u'weight': 1, u'source': 4}, {u'lldpDelay': 0.002254962921142578, u'delay': 0.001489400863647461, u'target': 3, u'weight': 1, u'source': 4}, {u'delay': 0, u'source': 4, u'target': 4, u'weight': 0}], u'multigraph': False}, u'bw': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}], u'links': [{u'source': 0, u'target': 0, u'weight': 0}, {u'bw_usage': 0.9593604263827729, u'target': 1, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 0, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9593604263827729, u'target': 2, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 0, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9593604263827729, u'target': 0, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 1, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'source': 1, u'target': 1, u'weight': 0}, {u'bw_usage': 1.1192538307795985, u'target': 2, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 1, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1192538307795985, u'target': 4, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 1, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9593604263827729, u'target': 0, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 2, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1192538307795985, u'target': 1, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 2, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'source': 2, u'target': 2, u'weight': 0}, {u'bw_usage': 1.1192538307795985, u'target': 3, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 2, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1192538307795985, u'target': 2, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 3, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'source': 3, u'target': 3, u'weight': 0}, {u'bw_usage': 0.9593604263827729, u'target': 4, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 3, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1192538307795985, u'target': 1, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 4, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9593604263827729, u'target': 3, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 4, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'source': 4, u'target': 4, u'weight': 0}], u'multigraph': False}}

# example 2
# 	network = {u'new_flows': {u"('10.0.0.1', '10.0.0.5')": {u'path': [1, 2, 5], u'type': u'S'}},u'flow_speed_priority': {u'1': {u"(1, '10.0.0.1', '10.0.0.5')": [787833.0], u"(1, '10.0.0.5', '10.0.0.1')": [16023.5]},u'3': {},u'2': {u"(1, '10.0.0.1', '10.0.0.5')": [738221.5], u"(1, '10.0.0.5', '10.0.0.1')": [16023.5]},u'5': {u"(1, '10.0.0.1', '10.0.0.5')": [737843.0], u"(1, '10.0.0.5', '10.0.0.1')": [16040.0]},u'4': {}}, u'graph': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5},{u'id': u'10.0.0.5'}, {u'id': u'10.0.0.1'}],u'links': [{u'source': 0, u'target': 0, u'weight': 0},{u'source': 0, u'target': 1, u'weight': 1},{u'source': 0, u'target': 2, u'weight': 1},{u'source': 0, u'mac_add': u'6a:55:af:17:bf:99', u'weight': 1,u'dpid_port': 3, u'target': 6},{u'source': 1, u'target': 0, u'weight': 1},{u'source': 1, u'target': 1, u'weight': 0},{u'source': 1, u'target': 2, u'weight': 1},{u'source': 1, u'target': 4, u'weight': 1},{u'source': 2, u'target': 0, u'weight': 1},{u'source': 2, u'target': 1, u'weight': 1},{u'source': 2, u'target': 2, u'weight': 0},{u'source': 2, u'target': 3, u'weight': 1},{u'source': 3, u'target': 2, u'weight': 1},{u'source': 3, u'target': 3, u'weight': 0},{u'source': 3, u'target': 4, u'weight': 1},{u'source': 4, u'mac_add': u'7e:4c:59:76:31:02', u'weight': 1,u'dpid_port': 3, u'target': 5},{u'source': 4, u'target': 1, u'weight': 1},{u'source': 4, u'target': 3, u'weight': 1},{u'source': 4, u'target': 4, u'weight': 0},{u'source': 5, u'mac_add': u'7e:4c:59:76:31:02', u'weight': 1,u'dpid_port': 3, u'target': 4},{u'source': 6, u'mac_add': u'6a:55:af:17:bf:99', u'weight': 1,u'dpid_port': 3, u'target': 0}], u'multigraph': False},u'flow_speed': {u'1': {u"('10.0.0.1', '10.0.0.5')": [6302.664], u"('10.0.0.5', '10.0.0.1')": [128.188]},u'3': {},u'2': {u"('10.0.0.1', '10.0.0.5')": [5905.772], u"('10.0.0.5', '10.0.0.1')": [128.188]}, u'5': {u"('10.0.0.1', '10.0.0.5')": [5902.744], u"('10.0.0.5', '10.0.0.1')": [128.32]}, u'4': {}}, u'delay': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4},{u'id': 5}],u'links': [{u'delay': 0, u'source': 0, u'target': 0, u'weight': 0},{u'lldpDelay': 0.28536009788513184,u'delay': 0.19464635848999023, u'target': 1,u'weight': 1, u'source': 0},{u'lldpDelay': 0.0026998519897460938,u'delay': 0.001606583595275879, u'target': 2, u'weight': 1, u'source': 0},{u'lldpDelay': 0.0030601024627685547,u'delay': 0.0015273094177246094, u'target': 0,u'weight': 1, u'source': 1},{u'delay': 0, u'source': 1, u'target': 1, u'weight': 0},{u'lldpDelay': 0.002644062042236328,u'delay': 0.0009990930557250977, u'target': 2,u'weight': 1, u'source': 1},{u'lldpDelay': 0.006080150604248047,u'delay': 0.004575967788696289, u'target': 4,u'weight': 1, u'source': 1},{u'lldpDelay': 0.00257110595703125, u'delay': 0.001624464988708496, u'target': 0,u'weight': 1, u'source': 2},{u'lldpDelay': 0.004813194274902344,u'delay': 0.0011938810348510742, u'target': 1,u'weight': 1, u'source': 2},{u'delay': 0, u'source': 2, u'target': 2, u'weight': 0},{u'lldpDelay': 0.002846956253051758,u'delay': 0.0015285015106201172, u'target': 3, u'weight': 1, u'source': 2},{u'lldpDelay': 0.0036110877990722656,u'delay': 0.0011713504791259766, u'target': 2,u'weight': 1, u'source': 3},{u'delay': 0, u'source': 3, u'target': 3, u'weight': 0},{u'lldpDelay': 0.002599954605102539,u'delay': 0.0016535520553588867, u'target': 4, u'weight': 1, u'source': 3},{u'lldpDelay': 0.0023660659790039062,u'delay': 0.0013778209686279297, u'target': 1,u'weight': 1, u'source': 4},{u'lldpDelay': 0.0035660266876220703, u'delay': 0.002362370491027832, u'target': 3,u'weight': 1, u'source': 4},{u'delay': 0, u'source': 4, u'target': 4,u'weight': 0}], u'multigraph': False},u'bw': {u'directed': True, u'graph': {},u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}],u'links': [{u'source': 0, u'target': 0, u'weight': 0}, {u'bw_usage': 1.0560000000004948, u'target': 1, u'weight': 1,u'util_ratio': 0.00010560000000004947, u'source': 0, u'bandwidth': 9998.944, u'cost': 1, u'congested': False},{u'bw_usage': 1.0560000000004948, u'target': 2, u'weight': 1,u'util_ratio': 0.00010560000000004947, u'source': 0, u'bandwidth': 9998.944,u'cost': 1, u'congested': False},{u'bw_usage': 1.0560000000004948, u'target': 0, u'weight': 1,u'util_ratio': 0.00010560000000004947, u'source': 1, u'bandwidth': 9998.944,u'cost': 1, u'congested': False}, {u'source': 1, u'target': 1, u'weight': 0}, {u'bw_usage': 1.0555777688932722, u'target': 2, u'weight': 1, u'util_ratio': 0.00010555777688932721, u'source': 1, u'bandwidth': 9998.944422231107,u'cost': 1, u'congested': False},{u'bw_usage': 1.0555777688932722, u'target': 4, u'weight': 1, u'util_ratio': 0.00010555777688932721, u'source': 1, u'bandwidth': 9998.944422231107,u'cost': 1, u'congested': False}, {u'bw_usage': 1.0560000000004948, u'target': 0, u'weight': 1,u'util_ratio': 0.00010560000000004947, u'source': 2, u'bandwidth': 9998.944,u'cost': 1, u'congested': False}, {u'bw_usage': 1.0555777688932722, u'target': 1, u'weight': 1, u'util_ratio': 0.00010555777688932721, u'source': 2, u'bandwidth': 9998.944422231107,u'cost': 1, u'congested': False}, {u'source': 2, u'target': 2, u'weight': 0}, {u'bw_usage': 1.0555777688932722, u'target': 3, u'weight': 1, u'util_ratio': 0.00010555777688932721, u'source': 2, u'bandwidth': 9998.944422231107,u'cost': 1, u'congested': False},{u'bw_usage': 1.0555777688932722, u'target': 2, u'weight': 1,u'util_ratio': 0.00010555777688932721, u'source': 3, u'bandwidth': 9998.944422231107,u'cost': 1, u'congested': False}, {u'source': 3, u'target': 3, u'weight': 0}, {u'bw_usage': 1.1515393842455524, u'target': 4, u'weight': 1,u'util_ratio': 0.00011515393842455524, u'source': 3, u'bandwidth': 9998.848460615754,u'cost': 1, u'congested': False},{u'bw_usage': 1.0555777688932722, u'target': 1, u'weight': 1,u'util_ratio': 0.00010555777688932721, u'source': 4, u'bandwidth': 9998.944422231107, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1515393842455524, u'target': 3, u'weight': 1, u'util_ratio': 0.00011515393842455524, u'source': 4, u'bandwidth': 9998.848460615754, u'cost': 1, u'congested': False}, {u'source': 4, u'target': 4, u'weight': 0}], u'multigraph': False}}

	get_graph_data(network)
	find_in_out_edges()
	cplex_mode()

	# print("%s, %s", net_data.graph_bw[1][2]['cost'])



def get_graph_data(network):
	print

	graph_host = json_graph.node_link_graph(network["graph"])

	graph_bw = json_graph.node_link_graph(network["bw"])

	net_data.edges = remove_self_loops(graph_host)  # set of edges in
	net_data.graph_bw = graph_bw  # the network graph with link_vector(cost, bandwidth, utilization Ratio, bandwidth usage, congestion status)
	flow_speed = network["flow_speed"]
	new_flows = network['new_flows']  # contains the {flows:{path:[], flowType: {S/NS}}
	net_data.flows = get_flows(new_flows)

	net_data.flow_demands = get_flow_demands(new_flows, flow_speed)
	print(net_data.flow_demands)
	get_nodes(graph_host.nodes())
	net_data.non_splitable_flows = net_data.flows.copy()

	net_data.display_net_data()


# the graph

# print(list(graph_bw.edges(data=True)))

#
#
# # print(new_flows)
# links_delay = json_graph.node_link_graph(network['delay'])
# print
# edges = remove_self_loops(graph_host)
# print
# print(edges)
# print
# edges = graph_bw.edges(data=True)
# print(graph_bw[769][257])
# print(graph_bw.get_edge_data(769, 717))
# print("bandwidth")
# # print(edges)
# # edges = links_delay.edges(data=True)
# print("\n links delay")
# # print(edges)
#
# # print(flow_sped.edges(data=True))
# print
# print(graph_bw)
def get_nodes(nodes):
	for n in nodes:
		net_data.nodes[n]  # adds the nodes in the dictionary


def get_flow_demands(new_flows, flow_speed):
	fd = defaultdict(list)
	flow_demand = {}

	for dp in flow_speed:
		for f in flow_speed[dp]:
			demands = flow_speed[dp][f]
			fd[f] = fd[f] + demands

	print(fd)
	for f in fd:
		flow = ast.literal_eval(f)
		avg_speed = np.average(fd[f])
		flow_demand[flow] = avg_speed

	return flow_demand


def get_flows(new_flows):
	flows = set()
	for f in new_flows:
		fl = ast.literal_eval(f)
		print fl
		flows.add(ast.literal_eval(f))
	return flows


def remove_self_loops(graph):
	loops = graph.selfloop_edges()
	graph.remove_edges_from(loops)
	edges = set(graph.edges())
	return edges


def send_msg(url, msg):
	send_msg = requests.put(url, json=msg)
	print(send_msg)

	if send_msg.status_code != 200:
		print("[PUT][Error] {}".format(send_msg.status_code))
	else:
		print("The send message was successful {}".format(send_msg.json()))
	# if send_msg.status_code != 201

def res_routing():
	request_get(URL)

	# while True:
	# 	print("While loop...")
	# 	request_get(URL)
	# 	print("going to take a nap now...")
	# 	time.sleep(2)
	# 	print("I just woke up....")
	# 	# send_msg(url, msg)


if __name__ == '__main__':
	res_routing()
