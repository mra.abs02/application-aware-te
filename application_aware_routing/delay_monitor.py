
# XXX The switch module is modified topology discovery code. It contains the delay calculation.

import time
import copy

from ryu.base import app_manager
from ryu.base.app_manager import  lookup_service_brick
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.controller import ofp_event
from ryu.ofproto import ofproto_v1_3
from ryu.lib import hub
from ryu.topology import switches
from ryu.topology.switches import LLDPPacket

from networkx.readwrite import json_graph


SENDING_ECHO_INTERVAL = 0.05 # to avoid overwhelming the switches
DISPLAY_DELAY = True
DELAY_MONITOR_INTERVAL = 1

class DelayMonitor(app_manager.RyuApp):

	OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

	def __init__(self, *args, **kwargs):
		super(DelayMonitor, self).__init__(*args, **kwargs)
		self.controller_switch_latency = {}
		self.name = "DelayMonitor"
		self.topology = lookup_service_brick('TopologyDiscovery')
		self.switch_module = lookup_service_brick('switches')
		self.graph_delay = {}

		self.datapaths = {}
		self.first_time = True  # this is first time that the module is running.
		self.start_time = time.time()

		self.delay_mon = hub.spawn(self._first_time)


	def get_delay_object(self, mode='object'):
		"""
			get the delay graph object.
		:param mode: whether covert it to json or not.
		:return:
		"""
		self.logger.info("get_delay is called in {} mode....".format(mode))
		if mode == "json":
			json_topo = json_graph.node_link_data(self.graph_delay)
			return json_topo
		else:
			return self.graph_delay

	@set_ev_cls(ofp_event.EventOFPStateChange, [MAIN_DISPATCHER, DEAD_DISPATCHER])
	def _state_change_handler(self, ev):
		datapath = ev.datapath
		if ev.state == MAIN_DISPATCHER:
			if not datapath.id in self.datapaths:
				self.logger.info('[DEBUG][DelayMon][stateChange()] Registering switch %016x', datapath.id)
				self.datapaths[datapath.id] = datapath
		elif ev.state == DEAD_DISPATCHER:
			if datapath.id in self.datapaths:
				self.logger.info('[DEBUG][DelayMon][stateChange()] Deregistering switch %016x', datapath.id)
				del self.datapaths[datapath.id]

	# def _create_graph_object_copy(self):
	# 	try:
	# 		current_time = time.time()
	# 		if current_time - self.start_time < self.topology.initial_delay + 2:
	# 			self.logger.info("[DEBUG][DelayMon][copy_graph_obj] Wait a moment .... ")
	# 			return False
	# 		else:
	# 			if self.topology.graph:
	# 				self.graph_delay = copy.deepcopy(self.topology.graph)
	# 				print(self.topology.graph.edges())
	# 				self.logger.info("Delay monitor, successfully started...")
	# 				return True
	# 			else:
	# 				print("topology error")
	# 				return False
	# 			# self.graph_delay = self.topology.graph.copy()
	#
	#
	# 	except:
	# 		if self.topology is None:
	# 			self.topology = lookup_service_brick("TopologyDiscovery")
	# 		self.logger.info("[Error][DelyMon][save_lldp_delay()] Could not save lldp_delay in the topology graph.")
	# 		return False

	def _create_graph_object_copy(self):
		try:


			if self.topology.graph:
				self.graph_delay = copy.deepcopy(self.topology.graph)
				print(self.topology.graph.edges())
				self.logger.info("Delay monitor, successfully started...")
				return True
			else:
				print("topology error")
				return False
				# self.graph_delay = self.topology.graph.copy()

		except:
			if self.topology is None:
				self.topology = lookup_service_brick("TopologyDiscovery")
			self.logger.info("[Error][DelyMon][save_lldp_delay()] Could not save lldp_delay in the topology graph.")
			return False


	def _first_time(self):
		while True:
			try:
				current_time = time.time()
				if current_time - self.start_time < self.topology.initial_delay + 2:
					hub.sleep(1)
				else:
					self.delay_mon = hub.spawn(self._delay_mon)
					break
			except:
				if self.topology is None:
					self.topology = lookup_service_brick("TopologyDiscovery")
				hub.sleep(1)




	def _delay_mon(self):

		i = 0
		while True:
			if self.first_time:
				successful = self._create_graph_object_copy()
				if successful:
					self.first_time = False
					print(self.graph_delay.edges(data=True))
				else:
					hub.sleep(1)
					continue

			if i == 2:
				self._send_echo_request()
				self._save_link_delay()
				self.show_delay_statics()
				i = 0
			hub.sleep(DELAY_MONITOR_INTERVAL)  # XXX 5
			i = i + 1

	# def _delay_mon(self):
	#
	# 	i = 0
	# 	while True:
	# 		if self.first_time:
	# 			successful = self._create_graph_object_copy()
	# 			if successful:
	# 				self.first_time = False
	# 				print(self.graph_delay.edges(data=True))
	# 			else:
	# 				hub.sleep(1)
	# 				continue
	#
	# 		self.show_delay_statics()
	# 		if i == 2:
	# 			# self._send_echo_request()
	# 			# self._save_link_delay()
	# 			i = 0
	# 		hub.sleep(DELAY_MONITOR_INTERVAL)  # XXX 5
	# 		i = i + 1

	def _send_echo_request(self):
		"""
			It sends echo request message to the switches in then network. It is used to get the delay between a switch
			and the controller.
		"""
		for datapath in self.datapaths.values():
			parser = datapath.ofproto_parser

			echo_request = parser.OFPEchoRequest(datapath, data="%.12f" % time.time())
			# self.logger.info("[DEBUG][DelayMon][echo_request()] data %s", echo_request)

			datapath.send_msg(echo_request)
			hub.sleep(SENDING_ECHO_INTERVAL)

	@set_ev_cls(ofp_event.EventOFPEchoReply, MAIN_DISPATCHER)
	def _echo_reply_handler(self, ev):
		current_time = time.time()
		try:
			# self.logger.info("[DEBUG][DelayMon][echo_reply_handler()] %s, eval %s ", ev.msg.data, eval(ev.msg.data))

			latency = current_time - float(ev.msg.data)
			self.controller_switch_latency[ev.msg.datapath.id] = latency
			# self.logger.info("[DEBUG][DelayMon][echo_reply_handler()] controller_switch_latency %s", self.controller_switch_latency)
		except:
			self.logger.error(
				"[Error][DelayMon][echo_req_handler()] "
				"An error occurred while calculating the delay between the switches and Controller."
			)
			return
	@set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
	def _packet_in_handler(self, ev):
		if self.first_time:
			return
		msg = ev.msg
		try:
			src_dpid, src_port_no = LLDPPacket.lldp_parse(msg.data)
			dpid = msg.datapath.id
			if self.switch_module is None:
				self.switch_module = lookup_service_brick('switches')

			for port in self.switch_module.ports.keys():
				if src_dpid == port.dpid and src_port_no == port.port_no:
					delay = self.switch_module.ports[port].delay
					self._save_lldp_delay(src=src_dpid, dst=dpid, lldp_delay=delay)

		except LLDPPacket.LLDPUnknownFormat as e:
			self.logger.error(
				"[Error][DelayMon][packet_in_handler()] "
				"An error occurred while while getting lldp delay."
			)
			return

	def _save_lldp_delay(self, src, dst, lldp_delay):
		# self.logger.info("src %s, dst %s, lldp %s", src, dst, lldp_delay)
		# print(self.graph_delay[src][dst])
		# self.graph_delay[src][dst]['lldpDelay'] = lldp_delay
		try:
			# if self.topology is None:
			# 	self.topology = lookup_service_brick('TopologyDiscovery')


			self.graph_delay[src][dst]['lldpDelay'] = lldp_delay
			# link_delay = self.calculate_link_delay(src, dst)
			# self.graph_delay[src][dst]['delay'] = link_delay

		except:
			if self.topology is None:
				self.topology = lookup_service_brick("TopologyDiscovery")
			self.logger.info("[Error][DelyMon][save_lldp_delay()] Could not save lldp_delay in the topology graph.")
			return

	def _save_link_delay(self):
		try:
			for src in self.graph_delay:
				for dst in self.graph_delay[src]:
					if src == dst:
						self.graph_delay[src][dst]['delay'] = 0
						continue

					link_delay = self.calculate_link_delay(src, dst)
					self.graph_delay[src][dst]['delay'] = link_delay


		except:
			self.logger.info("[Error][DelyMon][save_link_delay()] Could not save lldp_delay in the topology graph.")
			if self.topology is None:
				self.topology = lookup_service_brick("TopologyDiscovery")

		# self.graph_delay = copy.deepcopy(self.topology.graph)

	def calculate_link_delay(self, src, dst):
		try:
			fwd_delay = self.graph_delay[src][dst]['lldpDelay']
			return_delay = self.graph_delay[src][dst]['lldpDelay']
			src_cont_latency = self.controller_switch_latency[src]
			dst_cont_latency = self.controller_switch_latency[dst]

			link_delay = (fwd_delay + return_delay - src_cont_latency - dst_cont_latency) / 2
			# self.logger.info("[Debug][DelyMon][calculate_delay()] %s to %s = %s", src, dst, link_delay)

			return max(link_delay, 0)
		except:
			return float('inf')


	# def show_delay_statics(self):
	# 	if self.topology is not None and DISPLAY_DELAY:
	# 		self.logger.info("\nsrc   dst      delay")
	# 		self.logger.info("---------------------------")
	# 		for src in self.topology.graph:
	# 			for dst in self.topology.graph[src]:
	# 				delay = self.topology.graph[src][dst]['delay']
	# 				self.logger.info("%s<-->%s : %s" % (src, dst, delay))

	def show_delay_statics(self):
		if self.graph_delay is not None and DISPLAY_DELAY:
			self.logger.info("\nsrc   dst      delay")
			self.logger.info("---------------------------")
			for src in self.graph_delay:
				for dst in self.graph_delay[src]:
					delay = self.graph_delay[src][dst]['delay']
					self.logger.info("%s<-->%s : %s" % (src, dst, delay))
