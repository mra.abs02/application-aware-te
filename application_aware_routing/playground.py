import mysql.connector
import csv

mydb = mysql.connector.connect(
  host="localhost",
  user="mra",
  passwd="mra147",
  database="ryu"
)

mycursor = mydb.cursor()
# mycursor.execute("DROP TABLE IF EXISTS master")
# mycursor.execute("CREATE TABLE master (id int NOT NULL AUTO_INCREMENT PRIMARY KEY,  dpid int(11) NOT NULL,  timeid varchar(10) NOT NULL,  address varchar(100) NOT NULL, speed double NOT NULL )")

# sql =  "INSERT INTO master (id, dpid, timeid, address, speed) VALUES (%s, %s, %s, %s, %s)"
# val = [(1, 1, 'T1', '10.0.0.1', 2),
# (2, 2, 'T1', '10.0.0.1', 3),
# (3, 3, 'T1', '10.0.0.1', 4),
# (4, 1, 'T1', '10.0.0.2', 1),
# (5, 2, 'T1', '10.0.0.2', 2.5),
# (6, 3, 'T1', '10.0.0.2', 3.5),
# (7, 1, 'T2', '10.0.0.1', 1.5),
# (8, 2, 'T2', '10.0.0.1', 2.3),
# (9, 3, 'T2', '10.0.0.1', 3.3)]
#
# mycursor.executemany(sql, val)
# mydb.commit()

# mycursor.execute("SELECT * FROM tp ")
# for x in mycursor:
# 	print(x)
# print
#
#
# mycursor.execute("SELECT * FROM util ")
# for x in mycursor:
# 	print(x)
# print

# mycursor.execute("SELECT address, timeid, avg(speed) FROM master group by timeid, address")
# data = mycursor.fetchall()
# with open('mydata.json', 'w') as outfile:
# 	json.dump(data, outfile, indent=4)
c = csv.writer(open("tp.csv","wb"))

mycursor.execute("SELECT address, timeid, avg(speed) FROM tp group by timeid, address")
for x in mycursor:
	c.writerow(x)
	print(x)

c1 = csv.writer(open("util.csv","wb"))
# mycursor.execute("SELECT edge, timeid, util FROM util group by timeid, edge")
mycursor.execute("SELECT timeid, avg(util) FROM util group by timeid")
for x in mycursor:
	c1.writerow(x)
	print(x)