# TODO The new_flows doesn't contain the reverse path, I should fix this or it will cause problems
# coding=utf-8


import json
from webob import Response
import networkx as nx
import copy
from collections import defaultdict
import random
import ast

from ryu.app.wsgi import ControllerBase, WSGIApplication, route
from ryu.lib import dpid as dpid_lib
from ryu.base import app_manager
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import HANDSHAKE_DISPATCHER, CONFIG_DISPATCHER
from ryu.ofproto import ofproto_v1_3
from ryu.controller import ofp_event
from ryu.controller.handler import set_ev_cls
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import arp
from ryu import utils

import topology_discovery
import bandwidth_monitor

import delay_monitor

net_Optimizer_instance_name = "net_optimiz_api"
url = '/no/mac/{data}'
MULTIPATH_ROUTING = False


class NetOptimizAPIController(ControllerBase):
	def __init__(self, req, link, data, **config):
		super(NetOptimizAPIController, self).__init__(req, link, data, **config)
		self.net_optimiz_app = data[net_Optimizer_instance_name]


	@route("NetOptimizer", url, methods=['GET'],
		   requirements=None)  # The requirements is set to none, change if necessary
	def get_method(self, req, **kwargs):
		net_optimiz = self.net_optimiz_app
		net_optimiz.logger.info("[DEBUG][net_optimiz_api] Net_optimizer_rest_Api is called")
		topology = net_optimiz.get_network_info()
		print("network_topo_graph ", topology)
		body = json.dumps(topology)
		return Response(content_type="application/json", body=body)

	@route("NetOptimizer", url, methods=['PUT'], requirements=None)
	def put_method(self, req, **kwargs):
		net_optimize = self.net_optimiz_app
		data = kwargs['data']
		net_optimize.logger.info("[DEBUG][NetOptApi] the received data is: %s and data %s" % (req, data))

		try:
			entry = req.json if req.body else {}
		except ValueError:
			return Response(status=404)

		# try:
		splitable_path, non_splitable = net_optimize.set_path_dic(entry)
		net_optimize.install_all_reset_paths(splitable_path, non_splitable)
		body = json.dumps({'received': True})
		return Response(content_type="application/json", body=body)
		# except Exception as e:
		# 	net_optimize.logger.info("[DEBUG][NetOptApi] Exception: %s", e)
		# 	return Response(status=505)


class NetworkOptimizerRest(app_manager.RyuApp):
	"""
		NetworkOptimizerRest   is a Ryu app for forwarding packets in shortest
		path.
		The shortest path computation is done by module network awareness,
		network monitor and network delay detector.
	"""

	OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
	_CONTEXTS = {
		"topology_discovery": topology_discovery.TopologyDiscovery,
		"bandwidth_monitor": bandwidth_monitor.BandwidthMonitor,
		"delay_monitor": delay_monitor.DelayMonitor,
		"wsgi": WSGIApplication,
	}

	def __init__(self, *args, **kwargs):
		super(NetworkOptimizerRest, self).__init__(*args, **kwargs)
		self.datapaths = {}
		wsgi = kwargs['wsgi']
		self.topo = kwargs["topology_discovery"]
		self.bw_mon = kwargs["bandwidth_monitor"]
		self.delay_mon = kwargs["delay_monitor"]

		wsgi.register(NetOptimizAPIController, {net_Optimizer_instance_name: self})
		self.sample_dictionary = {}
		self.paths = {}
		self.new_flows = {}	# new_flows{str((src, dst)):{path:[], type:(splitable, non-spit}}
		# TODO becarefule the keys of new_flows are string not tuple
		self.multipath_group_ids = {}
		self.group_ids = []
		self.edges_flow_amount = {}	 # the amount of flow between two sw
		self.total_flow_amount = {}  # Total out going flow for each sw


	@set_ev_cls(ofp_event.EventOFPErrorMsg,[HANDSHAKE_DISPATCHER, CONFIG_DISPATCHER, MAIN_DISPATCHER])
	def error_msg_handler(self, ev):
		msg = ev.msg
		self.logger.info('[Error][MAIN_APP][OFPErrorMsg] type=0x%02x, code=0x%02x, message=%s', msg.type, msg.code,
						  utils.hex_array(msg.data))


	def get_network_info(self):
		"""
			Collects and returns the network information. Used by the REST-API.
		:return: network information
		"""
		network = {}

		network['graph'] = self.topo.get_graph_object(mode="json")
		network['bw'] = self.bw_mon.get_graph_object_with_bw(mode="json")
		flow_speed_with_priority, flow_speed = self.bw_mon.get_flow_speed()
		network['flow_speed'] = flow_speed
		network['flow_speed_priority'] = flow_speed_with_priority
		network['new_flows'] = self.new_flows
		network['delay'] = self.delay_mon.get_delay_object(mode="json")

		return network

	def set_path_dic(self, entry):
		split = ast.literal_eval(entry['splitable'])
		non_split  = ast.literal_eval(entry['non_split'])
		self.edges_flow_amount = ast.literal_eval(entry['amount'])
		return split, non_split

	def calculate_total_flow_amount(self):
		"""
			calcualtes the total out going flow on each sw.
		:return:
		"""
		for f, v in self.edges_flow_amount.viewitems():
			print
			self.total_flow_amount.setdefault(f, {})
			print
			print(v)
			for sw in v:
				total = sum(v[sw].values())
				self.total_flow_amount[f][sw] = total
				print
			print(self.total_flow_amount)


	def install_all_reset_paths(self, splitable, non_split):
		print("------installing the paths -----")
		print("[DEBUG][MAIN] RESPT PATHS --> splitable_path: %s, non_splitable_path: %s " % (splitable, non_split))

		# calculates the total flow
		self.calculate_total_flow_amount()

		# Install the paths for splitable and non_splitable
		for flow, path in non_split.viewitems():
			print("flow %s, path %s" %(flow , path))
			self.install_rest_path(flow[0], flow[1], path)



		for flow, spaths in splitable.viewitems():
			print("The splitables flows %s, paths %s" %(flow, spaths))
			self.install_rest_path(flow[0], flow[1], spaths)

	def install_rest_path(self, src_ip, dst_ip, paths):
		"""
			Performs multipath forwarding usig ECMP and group table.

		"""
		# #PATHs  should be a list
		src_location = self.topo.get_host_location(src_ip)
		dst_location = self.topo.get_host_location(dst_ip)

		self.logger.info("[info][MAIN_APP][install_rest] paths %s, src_ip %s, src_loc %s, dst_ip %s, dst_loc %s",
						 paths, src_ip, src_location, dst_ip, dst_location)

		if paths is None:
			self.logger.info("[Error][rest_path installer] error in finding the paths or flow_info")
			return

		src_sw, dst_sw = src_location[0], dst_location[0]
		first_port = src_location[1]
		last_port = dst_location[1]

		# get the reverse path
		# rev_paths = []
		# for p in paths:
		# 	rev_paths.append(p[::-1])
		# print("src %s, dst %s, paths %s | REVERS PATH %s" % (src_ip, dst_ip, paths, rev_paths))
		#
		# # install flow entries to datapath on the paths using group table.
		nodes_ports, sw_port_weight = self.add_ports_to_paths(paths, first_port, last_port, (src_ip, dst_ip))
		self.logger.info("[DEBUG][MAIN_APP][install_rest], src_sw %s, dst_sw %s, 1p %s, 2p %s... nodes_ports %s",
						 src_sw, dst_sw, first_port, last_port, nodes_ports)
		out_port = self.install_group_paths(src_sw, first_port, dst_sw, last_port, src_ip, dst_ip, paths, nodes_ports, sw_port_weight)
		print("the outport is ", out_port)
		# # install the flow entries for the reverse paths.
		# nodes_ports = self.add_ports_to_paths(rev_paths, last_port, first_port)
		# self.install_group_paths(dst_sw, last_port, src_sw, first_port, dst_ip, src_ip, rev_paths, nodes_ports)


	@set_ev_cls(ofp_event.EventOFPStateChange, [MAIN_DISPATCHER, DEAD_DISPATCHER])
	def _state_change_handler(self, ev):
		"""
			Collect datapath information.
		"""
		datapath = ev.datapath
		if ev.state == MAIN_DISPATCHER:
			if datapath.id not in self.datapaths:
				self.logger.debug('register datapath: %016x', datapath.id)
				self.datapaths[datapath.id] = datapath
		elif ev.state == DEAD_DISPATCHER:
			if datapath.id in self.datapaths:
				self.logger.debug('unregister datapath: %016x', datapath.id)
				del self.datapaths[datapath.id]

	@set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
	def _packet_in_handler(self, ev):
		"""
			Calculates for each packet in. For know source-targets it calculates the shortest path, and installs the flows
			on the switches. If the source and destination is not known then it sends ARP.
		"""

		msg = ev.msg
		datapath = msg.datapath
		in_port = msg.match['in_port']
		pkt = packet.Packet(msg.data)
		arp_pkt = pkt.get_protocol(arp.arp)
		ip_pkt = pkt.get_protocol(ipv4.ipv4)

		if isinstance(arp_pkt, arp.arp):
			self.logger.debug("[DEBUG][MAIN_APP][PACKET_IN] ARP packet_in event")
			self.arp_forwarding(msg, arp_pkt.src_ip, arp_pkt.dst_ip)

		if isinstance(ip_pkt, ipv4.ipv4):
			self.logger.info("[DEBUG][MAIN_APP][PACKET_IN] IPv4 packet_in")
			if len(pkt.get_protocol(ethernet.ethernet)):
				# eth_type = pkt.get_protocol(ethernet.ethernet)[0].ethertype 		TODO I don't know why they used [0], cause it raises error.
				# The reason is that they used get_protocols() which returns a list of the protocols.
				eth_type = pkt.get_protocol(ethernet.ethernet).ethertype
				self.logger.debug("[DEBUG][MAIN_APP][PACKET_IN] the ethertype is: %d", eth_type)
				self.logger.debug("[DEBUG][MAIN_APP][PACKET_IN] ip_pkt.src %s , ip_pkt.dst %s", ip_pkt.src, ip_pkt.dst)
				if MULTIPATH_ROUTING:
					self.multipath_forwarding(msg, eth_type, ip_pkt.src, ip_pkt.dst)
				else:
					self.shortest_forwarding(msg, eth_type, ip_pkt.src, ip_pkt.dst)

	def multipath_forwarding(self, msg, eth_type, src_ip, dst_ip):
		"""
			Performs multipath forwarding usig ECMP and group table.

		"""
		datapath = msg.datapath
		ofproto = datapath.ofproto
		parser = datapath.ofproto_parser
		in_port = msg.match['in_port']

		buffer_id = msg.buffer_id
		data = msg.data

		paths, src_location, dst_location = self.get_multiple_path(src_ip, dst_ip, 2)  # should I pass the src_ip and dst ip?
		self.logger.info("[info][multipath_fw] paths %s, src_ip %s, src_loc %s, dst_ip %s, dst_loc %s",
						 paths, src_ip, src_location, dst_ip, dst_location)

		if paths is None:
			self.logger.info("[Error][multipath_fw] error in finding the paths or flow_info")
			return
		src_sw, dst_sw = src_location[0], dst_location[0]
		first_port = src_location[1]
		last_port = dst_location[1]

		flow_info = (eth_type, src_ip, dst_ip, in_port)
		self.logger.info("[DEBUG][MAIN_APP][multipath_fw] flow_info (%s), src_sw %s, dst_sw %s, 1p %s, 2p %s.",
						 flow_info, src_sw, dst_sw, first_port, last_port)
		# get the reverse path
		rev_paths = []
		for p in paths:
			rev_paths.append(p[::-1])
		print("src %s, dst %s, paths %s | REVERS PATH %s"%(src_ip, dst_ip, paths, rev_paths))

		# install flow entries to datapath on the paths using group table.
		nodes_ports = self.add_ports_to_paths(paths, first_port, last_port)
		out_port = self.install_group_paths(src_sw, first_port, dst_sw, last_port, src_ip, dst_ip, paths, nodes_ports)

		# install the flow entries for the reverse paths.
		nodes_ports = self.add_ports_to_paths(rev_paths, last_port, first_port)
		self.install_group_paths(dst_sw, last_port, src_sw, first_port, dst_ip, src_ip, rev_paths, nodes_ports)

		# first_dp = self.datapaths[src_sw]
		self.logger.info("[addPorts2path] outport %s", out_port)

		self.send_packet_out(datapath, buffer_id, in_port, out_port, data)
		return

	def get_bucket_weight(self):
		pass

	def install_group_paths(self, src, first_port, dst, last_port, src_ip, dst_ip, paths, nodes_ports, weight_dict):
		"""
			It installs the multipaths using group table. However, Unlike the original code I ignore the
			 routing for ARP packets
		"""

		self.logger.info("[install_group] path %s, src %s, 1P %s, dst %s, 2p %s, sip %s dip %s",
						 paths, src, first_port, dst, last_port, src_ip, dst_ip)


		pw = []
		for path in paths:
			pw.append(len(path)-1)
			print path, "cost = ", pw[len(pw) - 1]

		paths_with_ports = nodes_ports
		switches_in_paths = set().union(*paths)
		self.logger.info("[addPorts2path] paths_with_port %s, switches_in_path %s, pw %s",
						 paths_with_ports, switches_in_paths, pw)

		for node in switches_in_paths:

			# dp = self.datapath_list[node]
			dp = self.datapaths[node]
			ofp = dp.ofproto
			ofp_parser = dp.ofproto_parser

			ports = defaultdict(list)
			actions = []
			i = 0

			for path in paths_with_ports:
				if node in path:
					in_port = path[node][0]
					out_port = path[node][1]

					# if (out_port, pw[i]) not in ports[in_port]:   <--- In the original code the paths are unique and link disjoint
					if out_port not in ports[in_port]:
						ports[in_port].append(out_port)
						# ports[in_port].append((out_port, pw[i]))
				i += 1
			self.logger.info("The ports for src %s - dst %s is  %s", src_ip, dst_ip, ports)

			for in_port in ports:

				match_ip = ofp_parser.OFPMatch(
					eth_type=0x0800,
					ipv4_src=src_ip,
					ipv4_dst=dst_ip
				)

				# !!!!!!!!! do I need to get use group table for arp also?
				match_arp = ofp_parser.OFPMatch(
				    eth_type=0x0806,
				    arp_spa=src_ip,
				    arp_tpa=dst_ip
				)

				out_ports = ports[in_port]
				print out_ports

				if len(out_ports) > 1:
					group_id = None
					group_new = False

					if (node, src, dst) not in self.multipath_group_ids:
						group_new = True
						self.multipath_group_ids[
							node, src, dst] = self.generate_openflow_gid()
					group_id = self.multipath_group_ids[node, src, dst]

					buckets = []
					# print "node at ",node," out ports : ",out_ports
					# for port, weight in out_ports:	<----- In the original code the paths are link disjoint
					for port in out_ports:
						# bucket_weight = int(round((1 - weight/sum_of_pw) * 10)) for now just use ECMP
						if weight_dict is not None:
							bucket_weight = weight_dict[node][port]
						else:
							bucket_weight = 50
						bucket_action = [ofp_parser.OFPActionOutput(port)]
						buckets.append(
							ofp_parser.OFPBucket(
								weight=bucket_weight,
								watch_port=port,
								watch_group=ofp.OFPG_ANY,
								actions=bucket_action
							)
						)

					if group_new:
						req = ofp_parser.OFPGroupMod(
							dp, ofp.OFPGC_ADD, ofp.OFPGT_SELECT, group_id,
							buckets
						)
						dp.send_msg(req)
						self.logger.info("[install_group] install new group mod. req %s, bucket %s",
										 req, buckets)

					else:
						req = ofp_parser.OFPGroupMod(
							dp, ofp.OFPGC_MODIFY, ofp.OFPGT_SELECT,
							group_id, buckets)
						dp.send_msg(req)
						self.logger.info("[install_group] modify group mod. req %s, bucket %s",
										 req, buckets)
					actions = [ofp_parser.OFPActionGroup(group_id)]

					self.add_flow(dp, 50000, match_ip, actions)
					self.add_flow(dp, 1, match_arp, actions)

				elif len(out_ports) == 1:
					actions = [ofp_parser.OFPActionOutput(out_ports[0])]
					self.logger.info("[install_group] install single flow mod. out_Ports %s", out_ports)

					self.add_flow(dp, 50000, match_ip, actions)
					self.add_flow(dp, 1, match_arp, actions)
		return paths_with_ports[0][src][1]


	def generate_openflow_gid(self):
		'''
		Returns a random OpenFlow group id
		'''
		n = random.randint(0, 2 ** 32)
		while n in self.group_ids:
			n = random.randint(0, 2 ** 32)

		self.logger.info("[GEN-GID] id = %s", n)
		return n
	def calculate_sw_port_weight(self, flow_info, src_sw, dst_sw):
		amount = self.edges_flow_amount[flow_info][src_sw][dst_sw]
		total_amount = self.total_flow_amount[flow_info][src_sw]
		ratio = (amount/total_amount) * 100
		return ratio

	def add_ports_to_paths(self, paths, first_port, last_port, flow_info=None):
		'''
		Add the ports that connects the switches for all paths
		'''
		if flow_info is not None:
			sw_port_weight = {}
		else:
			sw_port_weight = None
		link_to_port = self.topo.link_to_port
		self.logger.info("[addPorts2path] path %s, 1P %s, 2P %s", paths, first_port, last_port)

		paths_p = []
		for path in paths:
			p = {}
			in_port = first_port
			for s1, s2 in zip(path[:-1], path[1:]):
				ports = self.get_port_pair_from_link(link_to_port,
													 s1, s2)
				# self.logger.info("[addPorts2path] s1 %s, s2 %s, ports %s", s1, s2, ports)
				out_port = ports[0]  # this is the out_port of the current switch in the path
				if flow_info is not None:
					# calculate the ratio
					ratio = self.calculate_sw_port_weight(flow_info, s1, s2)
					sw_port_weight.setdefault(s1, {})
					sw_port_weight[s1][out_port] = ratio
				p[s1] = (in_port, out_port)
				in_port = ports[1]  # This is the in_port of the next switch in the path
			p[path[-1]] = (in_port, last_port)
			paths_p.append(p)
			# self.logger.info("[addPorts2path] paths_p %s", paths_p)
			print("weight: ", sw_port_weight)

		return paths_p, sw_port_weight

	def get_multiple_path(self, src_ip, dst_ip, k=2):
		"""
			Get k paths for a src, dst ip.
		:param src_ip:
		:param dst_ip:
		:param k:
		:return:
		"""

		paths = []
		src_location = self.topo.get_host_location(src_ip)
		dst_location = self.topo.get_host_location(dst_ip)

		self.logger.debug("[DEBUG][MAIN_APP][get_mult_path] src_location %s, dst_location %s, ",
						 src_location, dst_location)

		if src_location and dst_location:
			src_sw, dst_sw = src_location[0], dst_location[0]

			self.logger.debug(
				"[DEBUG][MAIN_APP][get_mult_path] sr_switches %s. dst_sw %s", src_sw, dst_sw)
			# self.logger.info(
			# 	"[DEBUG][MAIN_APP][get_mult_path] edge_sw_ports %s", edge_sw_ports)
			if dst_sw:
				paths = self.get_paths(src_sw, dst_sw, k)
				if paths is None:
					self.logger.info(
						"[Error][MAIN_APP][get_mult_path] No path is found.")
					return None, None, None
				self.logger.info(
					"[DEBUG][MAIN_APP][get_mult_path][PATH]%s<-->%s: %s" % (src_ip, dst_ip, paths))

			return paths, src_location, dst_location
		else:
			self.logger.info("[Error][get_mult_path] error in finding the paths or flow_info")

			return None, None, None

	def arp_forwarding(self, msg, src_ip, dst_ip):
		"""
			If the destination host exist, it forwards the ARP packet. Otherwise, flood the network.
		"""
		datapath = msg.datapath
		ofproto = datapath.ofproto
		parser = datapath.ofproto_parser

		host_location = self.topo.get_host_location(dst_ip)
		if host_location:
			dst_datapath, out_port = host_location[0], host_location[1]

			datapath = self.datapaths[dst_datapath]
			packet_out = self._build_packet_out(datapath, ofproto.OFP_NO_BUFFER, ofproto.OFPP_CONTROLLER,
												out_port, msg.data)
			self.logger.debug("[DEBUG][MAIN_APP][ARP_FW] ARP src %s, dst %s, dst_sw %s, port %s."
							 % (src_ip, dst_ip, dst_datapath, out_port))

			datapath.send_msg(packet_out)
			self.logger.debug("[DEBUG][MAIN_APP][ARP_FW] ARP packet is forwarded to the dst.")
		else:
			self.logger.debug("[DEBUG][MAIN_APP][ARP_FW] ARP packet is flooding.")

			self.flood(msg)

	def flood(self, msg):
		"""
		  Flood ARP packet to the switches' edge ports, where the hosts are connected.
		  those ports which doesn't have any record of the hosts.
        """
		datapath = msg.datapath
		ofproto = datapath.ofproto
		parser = datapath.ofproto_parser

		for dpid in self.topo.switch_edge_ports:
			for port in self.topo.switch_edge_ports[dpid]:
				if (dpid, port) not in self.topo.edge_switch_table.keys():
					datapath = self.datapaths[dpid]
					out = self._build_packet_out(datapath, ofproto.OFP_NO_BUFFER,
												 ofproto.OFPP_CONTROLLER, port, msg.data)
					datapath.send_msg(out)
			self.logger.debug("Flooding msg")

	def _build_packet_out(self, datapath, buffer_id, src_port, dst_port, data):
		"""
			Builds packet out object.
		"""
		actions = []
		if dst_port:
			actions.append(datapath.ofproto_parser.OFPActionOutput(dst_port))

		msg_data = None
		if buffer_id == datapath.ofproto.OFP_NO_BUFFER:
			if data is None:
				return None
			msg_data = data

		out = datapath.ofproto_parser.OFPPacketOut(datapath=datapath, buffer_id=buffer_id, data=msg_data,
												   in_port=src_port, actions=actions)
		return out

	def shortest_forwarding(self, msg, eth_type, src_ip, dst_ip):
		"""
			To calculate shortest forwarding path and install them into datapaths.

		"""
		datapath = msg.datapath
		ofproto = datapath.ofproto
		parser = datapath.ofproto_parser
		in_port = msg.match['in_port']

		src_dst_switches = self.get_sw(datapath.id, in_port, src_ip, dst_ip)
		self.logger.debug("[DEBUG][MAIN_APP][shortest_fw] sr_dst_switches %s. datapath.id %s, in_port= %s", src_dst_switches, datapath.id, in_port)

		if src_dst_switches:
			src_sw, dst_sw = src_dst_switches[0], src_dst_switches[1]
			self.logger.debug("[DEBUG][MAIN_APP][shortest_fw] sr_switches %s. dst_sw %s", src_sw, dst_sw)
			if dst_sw:
				path = self.get_paths(src_sw, dst_sw)[0]
				if path is None:
					self.logger.info("[Error][MAIN_APP][shortest_fw] No path is found.")
					return
				self.logger.info("[DEBUG][MAIN_APP][shortest_fw][PATH]%s<-->%s: %s" % (src_ip, dst_ip, path))
				flow_info = (eth_type, src_ip, dst_ip, in_port)
				self.logger.debug("[DEBUG][MAIN_APP][shortest_fw] flow_info (%s).", flow_info)

				# install flow entries to datapath on the path.
				self.install_flow(self.datapaths, self.topo.link_to_port, self.topo.edge_switch_table, path, flow_info, msg.buffer_id, msg.data)
		return

	def get_sw(self, dpid, in_port, src_ip, dst_ip):
		"""
			Get pair of source and destination switches.
		"""
		src_sw = dpid
		dst_sw = None

		src_location = self.topo.get_host_location(src_ip)

		if in_port in self.topo.switch_edge_ports[dpid]:
			self.logger.debug("[DEBUG][MAIN_APP][get_sw][if condition] is true")
			self.logger.debug("[DEBUG][MAIN_APP][get_sw] the dpid %s, in_port %s, the switch_edge_ports[dpid]: %s",
							 dpid, in_port, self.topo.switch_edge_ports[dpid])

			if (dpid, in_port) == src_location:
				src_sw = src_location[0]
				self.logger.info("[DEBUG][MAIN_APP][get_sw] src_loc %s, src_sw %s.", src_location, src_sw)
			else:
				return None
		# else:
		# 	return None

		dst_location = self.topo.get_host_location(dst_ip)
		self.logger.debug("[DEBUG][MAIN_APP][get_sw] dst_loc %s.", dst_location)

		if dst_location:
			dst_sw = dst_location[0]

		return src_sw, dst_sw

	def get_paths(self, src_sw, dst_sw, k=1):
		"""
			Get multiple shortest path if it exists, otherwise calculate it.
		"""
		self.logger.debug(
			"[DEBUG][MAIN_APP][get_path]paths between %s and %s" % (src_sw, dst_sw))
		try:
			shortest_paths = self.paths.get(src_sw).get(dst_sw)
			self.logger.debug(
				"[DEBUG][MAIN_APP][get_path]paths between %s and %s, are: %s" % (src_sw, dst_sw, shortest_paths))
			# shortest_path = shortest_paths[len(shortest_paths) - 1]  # TODO right now I will try to get the longest path. if it is not usefule get the shortest one
			shortest_path = shortest_paths[:k]  # TODO right now I will try to get the longest path. if it is not usefule get the shortest one
			self.logger.debug(
				"[DEBUG][MAIN_APP][get_path]shortest path between %s and %s, is: %s" % (src_sw, dst_sw, shortest_path))
			return shortest_path

		except:
			graph = copy.deepcopy(self.topo.graph)
			found_any = self.find_shortest_paths(graph, src_sw, dst_sw)
			if found_any:
				shortest_paths = self.paths.get(src_sw).get(dst_sw)
				self.logger.debug(
					"[DEBUG][MAIN_APP][get_path][excp]paths between %s and %s, are: %s" % (src_sw, dst_sw, shortest_paths))
				shortest_path = shortest_paths[:k] # TODO if the longest path doesn't work use shortest path.
				# shortest_path = shortest_paths[len(shortest_paths) - 1] # TODO for some reasons the  the longest path doens't work properly.
				self.logger.debug(
					"[DEBUG][MAIN_APP][get_path][excp]shortest path between %s and %s, is: %s" % (src_sw, dst_sw, shortest_path))
				return shortest_path
			else:
				self.logger.info(
					"[Error][MAIN_APP][get_path][excp]No shortest path between %s and %s, is available." % (
					src_sw, dst_sw))
				return None

		# elif weight == self.WEIGHT_MODEL['bw']:
		# 	# Because all paths will be calculate
		# 	# when call self.monitor.get_best_path_by_bw
		# 	# So we just need to call it once in a period,
		# 	# and then, we can get path directly.
		# 	try:
		# 		# if path is existed, return it.
		# 		path = self.monitor.best_paths.get(src).get(dst)
		# 		return path
		# 	except:
		# 		# else, calculate it, and return.
		# 		result = self.monitor.get_best_path_by_bw(graph,
		# 												  shortest_paths)
		# 		paths = result[1]
		# 		best_path = paths.get(src).get(dst)
		# 		return best_path

	def find_shortest_paths(self, graph, src_sw, dst_sw):
		"""
			Great K shortest paths of src to dst.
		"""
		graph = copy.deepcopy(graph)
		shortest_paths = list(nx.shortest_simple_paths(graph, source=src_sw,
											 target=dst_sw, weight='weight'))
		if shortest_paths:
			self.logger.debug("[DEBUG][MAIN_APP][shortest_path]paths with sw %s as the source %s" % (src_sw, self.paths.get(src_sw, None)))
			self.paths.setdefault(src_sw, {})
			self.paths[src_sw].setdefault(dst_sw, [])
			self.paths[src_sw][dst_sw] = shortest_paths
			self.logger.debug("[DEBUG][MAIN_APP][shortest_path]paths: %s" % self.paths)
			return True

		else:
			self.logger.debug("[DEBUG][MAIN_APP][shortest_path] No path between %s and %s" % (src_sw, dst_sw))
			return None

	def install_flow(self, datapaths, link_to_port, access_table, path, flow_info, buffer_id, data=None):
		'''
			Install flow entires for roundtrip: go and back.
			@parameter: path=[dpid1, dpid2...]
						flow_info=(eth_type, src_ip, dst_ip, in_port)
		'''
		if path is None or len(path) == 0:
			self.logger.info("Path error!")
			return
		in_port = flow_info[3]
		first_dp = datapaths[path[0]]
		out_port = first_dp.ofproto.OFPP_LOCAL
		back_info = (flow_info[0], flow_info[2], flow_info[1])




		self.logger.debug("--------------[DEBUG][MAIN_APP][install_flow]\n"
						 "datapath %s,\n in_port %s,\n L2P %s,\n access_table %s,\n path %s,\n flow_info %s\n, back_info %s"
						 "------------------------------------------------------- ",
						 datapaths, in_port, link_to_port, access_table, path, flow_info, back_info)

		# inter_link
		if len(path) > 2:

			for i in xrange(1, len(path) - 1):
				# self.logger.info("[DEBUG][MAIN_APP][install_flow] path > 2 | i = %s, flow %s, path %s, sw %s",
				# 				 i, flow_info, path, path[i])

				port = self.get_port_pair_from_link(link_to_port,
													path[i - 1], path[i])
				port_next = self.get_port_pair_from_link(link_to_port,
														 path[i], path[i + 1])
				if port and port_next:
					src_port, dst_port = port[1], port_next[0]
					datapath = datapaths[path[i]]
					self.send_flow_mod(datapath, flow_info, src_port, dst_port)
					self.send_flow_mod(datapath, back_info, dst_port, src_port)
					# self.logger.info("[DEBUG][MAIN_APP][install_flow] path > 2 |inter_link flow install")
		if len(path) > 1:
			# the last flow entry: tor -> host

			port_pair = self.get_port_pair_from_link(link_to_port,
													 path[-2], path[-1])
			# self.logger.info("[DEBUG][MAIN_APP][install_flow] path > 1 | flow %s, path %s, link(%s, %s)=port(%s)",
			# 				 flow_info, path, path[-2], path[-1], port_pair)

			if port_pair is None:
				self.logger.info("Port is not found")
				return
			src_port = port_pair[1]

			dst_port = self.get_port(flow_info[2], access_table)
			if dst_port is None:
				self.logger.info("Last port is not found.")
				return

			last_dp = datapaths[path[-1]]
			self.send_flow_mod(last_dp, flow_info, src_port, dst_port)
			self.send_flow_mod(last_dp, back_info, dst_port, src_port)

			# the first flow entry
			port_pair = self.get_port_pair_from_link(link_to_port,
													 path[0], path[1])
			if port_pair is None:
				self.logger.info("Port not found in first hop.")
				return
			out_port = port_pair[0]
			self.send_flow_mod(first_dp, flow_info, in_port, out_port)
			self.send_flow_mod(first_dp, back_info, out_port, in_port)
			self.send_packet_out(first_dp, buffer_id, in_port, out_port, data)

		# src and dst on the same datapath
		else:
			out_port = self.get_port(flow_info[2], access_table)
			self.logger.debug("[DEBUG][MAIN_APP][install_flow] else: outport %s, dst_ip: %s", out_port, flow_info[2])
			if out_port is None:
				self.logger.debug("Out_port is None in same dp")
				return
			self.send_flow_mod(first_dp, flow_info, in_port, out_port)
			self.send_flow_mod(first_dp, back_info, out_port, in_port)
			self.send_packet_out(first_dp, buffer_id, in_port, out_port, data)

		flow_key = str((flow_info[1], flow_info[2]))
		self.new_flows.setdefault(flow_key, {})
		self.new_flows[flow_key]['path'] = path

		# adding the reverse path --> BUT THIS MIGHT NOT BE NEEDED
		flow_key = str((flow_info[2], flow_info[1]))
		self.new_flows.setdefault(flow_key, {})
		rev_path = path[::-1]  # reversing the path
		self.new_flows[flow_key]['path'] = rev_path
		self.new_flows[flow_key]['type'] = "S"

		self.logger.info("[DEBUG][MAIN_APP][install_flow] The new_flows: %s", self.new_flows)


	def get_port_pair_from_link(self, link_to_port, src_dpid, dst_dpid):
		"""
			Get port pair of link, so that controller can install flow entry.
		"""
		if (src_dpid, dst_dpid) in link_to_port:
			# self.logger.info("dpid:%s->dpid:%s the ports are %s" % (src_dpid, dst_dpid, link_to_port[(src_dpid, dst_dpid)]))
			return link_to_port[(src_dpid, dst_dpid)]
		else:
			self.logger.info("dpid:%s->dpid:%s is not in links" % (
				src_dpid, dst_dpid))
			return None

	def send_flow_mod(self, datapath, flow_info, src_port, dst_port):
		"""
			Build flow entry, and send it to datapath.
		"""
		parser = datapath.ofproto_parser
		actions = []
		actions.append(parser.OFPActionOutput(dst_port))

		match = parser.OFPMatch(
			in_port=src_port, eth_type=flow_info[0],
			ipv4_src=flow_info[1], ipv4_dst=flow_info[2])
		# self.logger.info("[DEBUG][MAIN_APP][send_flow] flow sent for flow %s, src_port %s, dst_port %s, match %s, action %s"
		# 				 %(flow_info, src_port, dst_port, match, actions))

		self.add_flow(datapath, 1, match, actions,
					  idle_timeout=60, hard_timeout=60)
#					  # idle_timeout=15, hard_timeout=60)

	def add_flow(self, dp, p, match, actions, idle_timeout=0, hard_timeout=0):
		"""
			Send a flow entry to datapath.
		"""
		ofproto = dp.ofproto
		parser = dp.ofproto_parser

		inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
											 actions)]

		mod = parser.OFPFlowMod(datapath=dp, priority=p,
								idle_timeout=idle_timeout,
								hard_timeout=hard_timeout,
								match=match, instructions=inst)
		# self.logger.info("[DEBUG][MAIN_APP][add_flow] sent for flow mod with datapath %s, match: %s, inst %s ",
		# 				 dp, match, inst)

		dp.send_msg(mod)

	def get_port(self, dst_ip, access_table):
		"""
			Get access port if dst host.
			access_table: {(sw,port) :(ip, mac)}
		"""
		self.logger.info("[DEBUG][MAIN_APP][get_port] dst_ip %s, access_table %s", dst_ip, access_table)

		if access_table:
			if isinstance(access_table.values()[0], tuple):
				for key in access_table.keys():
					if dst_ip == access_table[key][0]:
						dst_port = key[1]
						return dst_port
		return None

	def send_packet_out(self, datapath, buffer_id, src_port, dst_port, data):
		"""
			Send packet out packet to assigned datapath.
		"""
		out = self._build_packet_out(datapath, buffer_id,
									 src_port, dst_port, data)
		if out:
			datapath.send_msg(out)