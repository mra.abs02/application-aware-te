# Topology Discovery Module
# TODO to remove the and update the topology curretly instead of callin it every 3 seconds, use events also use a paramieter
#  to distigues whether conteroller triggered the topo update or the network and if I am going to use the periodic polling
#  then I should reset the variables each time

import time
import copy

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.topology import event
from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib import hub
from ryu.topology.api import get_switch, get_link
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import ethernet

import networkx as nx
from networkx.readwrite import json_graph


INITIAL_DELAY = 2
DISPLAY_TOPO = True


class TopologyDiscovery(app_manager.RyuApp):
	"""
		This class provide functionality for network topology discovery.
	"""

	OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

	topology_events = [event.EventSwitchEnter, event.EventSwitchLeave,
					   event.EventPortAdd, event.EventPortDelete,
					   event.EventPortModify, event.EventLinkAdd,
					   event.EventLinkDelete
					   ]

	def __init__(self, *args, **kwargs):
		super(TopologyDiscovery, self).__init__(*args, **kwargs)
		self.name = "TopologyDiscovery"
		self.link_to_port = {} 			# {(src, dst):(src_port, dst_port} maps the links to the ports
		self.pre_link_to_port = {}
		self.interior_ports = {} 		# {dpid:set(ports)} contains the ports in the network that are connected to
		# a switch (not a host)
		self.edge_switch_table = {}  	# {(dpid, in_port): in_port},
										# It stores host info and to what port of a switch it is connected(~ access_table),
		self.switch_edge_ports = {} 	# {(dpid): (edge_ports)} it contains the port_no where the hosts are connected to
		self.pre_edge_switch_table = {}
		self.switch_port_table = {}  	# {dpid: in_port} It contains both of the interior and  edge ports
		self._discovery = hub.spawn(self._topology_discovery)
		self.switches = []				# Stores the switches dpid [dpid,...]
		self.graph = nx.DiGraph()  		# the network topology graph
		self.pre_graph = nx.DiGraph()  		# the network topology graph
		self.start_time = time.time()  	# the time controller started
		self.topology_api_app = self  	# the object of this class
		self.graph_with_hosts = nx.DiGraph()
		self.initial_delay = INITIAL_DELAY  # this is used to synchronize the modules.

	def _topology_discovery(self):
		i = 0
		while True:
			self.disply_topology()
			if i == 2:
				self.get_topology(None)
				i = 0
			hub.sleep(1)  # XXX 5
			i = i + 1

	def get_graph_object(self, mode='object'):
		self.logger.info("get_graph is called in {} mode....".format(mode))
		if mode == "json":
			json_topo = json_graph.node_link_data(self.graph_with_hosts)
			return json_topo
		else:
			return self.graph

	@set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
	def switch_feature_handler(self, ev):
		"""
			When a switch is connected for the first time to the controller, the controller installs the table miss
			entry.
		:param ev: OpenFlow SwitchFeatures event
		:return: None
		"""
		datapath = ev.msg.datapath
		ofproto = datapath.ofproto
		parser = datapath.ofproto_parser
		self.logger.info("[INFO][TopoDisc] switch with dpid: %s connected", datapath.id)

		match = parser.OFPMatch()
		actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER, ofproto.OFPCML_NO_BUFFER)]
		self.add_flow(datapath, 0, match, actions)

	def add_flow(self, datapath, priority, match, actions, idle_timeout=0, hard_timeout=0):
		"""
			Installs the flow mod on a datapath.
		"""
		ofproto = datapath.ofproto
		parser = datapath.ofproto_parser
		inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
		mod = parser.OFPFlowMod(datapath=datapath, priority=priority, instructions=inst, hard_timeout=hard_timeout,
								idle_timeout=idle_timeout, match=match)
		datapath.send_msg(mod)

	@set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
	def _packet_in_handler(self, ev):
		"""
			This is a private function for handling packet_in events.
		:param ev: Packet_in events
		"""
		msg = ev.msg
		datapath = msg.datapath
		in_port = msg.match['in_port']
		pkt = packet.Packet(msg.data)
		arp_pkt = pkt.get_protocol(arp.arp)
		ip_pkt = pkt.get_protocol(ipv4.ipv4)

		if arp_pkt:
			arp_src_ip = arp_pkt.src_ip
			src_mac = arp_pkt.src_mac
			self.logger.debug("[DEBUG][topology_discovery][packet_in] arp_src_ip: %s, src_mac: %s", arp_src_ip, src_mac)
			self.register_host_info(datapath.id, in_port, arp_src_ip, src_mac)
		elif ip_pkt:
			self.logger.debug("[DEBUG]ip packet %s", ip_pkt)
			ip_src_ip = ip_pkt.src
			# eth = pkt.get_protocol(ethernet.ethernet)[0]   TODO I don't know why they used [0], cause it raises error.
			src_eth = pkt.get_protocol(ethernet.ethernet).src
			self.logger.debug("[DEBUG][topology_discovery][packet_in] ip_src_ip: %s, src_eth: %s", ip_src_ip, src_eth)
			self.register_host_info(datapath.id, in_port, ip_src_ip, src_eth)
			self.add_host_to_graph()
		else:
			pass

	def register_host_info(self, dpid, in_port, src_ip, src_mac):
		"""
			Registers the host information. its IP address, mac address and to which switch it is connected
		:param dpid: edge switch id.
		:param in_port: the switch port the host is connected to.
		:param src_ip: the host's IP address.
		:param src_mac: the host's mac address.
		"""
		if in_port in self.switch_edge_ports[dpid]:
			# self.logger.info("[DEBUG][TOPOLOGYDiscovery][reg_host_info] edge_switch table %s", self.edge_switch_table)
			if (dpid, in_port) in self.edge_switch_table:
				if self.edge_switch_table[(dpid, in_port)] == (src_ip, src_mac):
					return
				else:
					self.edge_switch_table[(dpid, in_port)] = (src_ip, src_mac)
					# _graph.add_edge(dpid, src_ip, weight=1, host_mac=src_mac)
			else:
				self.edge_switch_table.setdefault((dpid, in_port), ())
				self.edge_switch_table[(dpid, in_port)] = (src_ip, src_mac)
				# _graph.add_edge(dpid, src_ip, weight=1, host_mac=src_mac)

			self.logger.debug("[DEBUG][TOPOLOGYDiscovery][reg_host_info] edge_switch table %s", self.edge_switch_table)

	def add_host_to_graph(self):
		"""
			This function added the host to the graph.
		:return:
		"""
		self.graph_with_hosts = copy.deepcopy(self.graph)
		for key in self.edge_switch_table:
			dpid, in_port = key[0], key[1]
			host_ip, mac_add = self.edge_switch_table[key][0], self.edge_switch_table[key][1]
			self.graph_with_hosts.add_edge(dpid, host_ip, weight=1, dpid_port=in_port, mac_add=mac_add)
			self.graph_with_hosts.add_edge(host_ip, dpid, weight=1, dpid_port=in_port, mac_add=mac_add)
			self.logger.debug("[DEBUG][TOPO_DISC][h2graph] dpid %s, in_port %s, host %s, mac %s",
							 dpid, in_port, host_ip, mac_add)

			self.logger.debug("[DEBUG][TOPO_DISC][h2graph] host_graph = %s", [self.graph_with_hosts.edges(data=True)])


	@set_ev_cls(topology_events)
	def get_topology(self, ev):
		"""
			Gets the topology of the network i.e., switches and links and creates the graph of the topology.
		:param ev:
		:return:
		"""
		current_time = time.time()
		if current_time - self.start_time < INITIAL_DELAY:
			self.logger.info("[DEBUG][TopoDisc][get_topo()] Wait a moment .... ")
			return

		self.logger.info("[DEBUG][TopoDisc] Get Topology.... ")
		switch_list = get_switch(self.topology_api_app, None)
		self.create_port_map(switch_list)
		self.switches = [sw.dp.id for sw in switch_list]  # differs
		# self.switches = self.switch_port_table.keys()
		links = get_link(self.topology_api_app, None)
		link_lists = [(link.src.dpid, link.dst.dpid) for link in links]

		self.create_interior_links(links)
		self.create_edge_ports()
		self.create_topo_graph(self.link_to_port.keys())
		# self.add_host_to_graph()  TODO I might need to called this one

		self.logger.debug("[DEBUG][TopoDiscovery][get_topo()] switches: %s,\n links:%s", self.switches, link_lists)

	def create_port_map(self, switch_list):
		"""
			It addes the ports of each switch to the self.switch_port_table{}
		:param switch_list: list of switches in the network
		:return:
		"""
		for sw in switch_list:
			dpid = sw.dp.id
			self.switch_port_table.setdefault(dpid, set())
			self.interior_ports.setdefault(dpid, set())
			self.switch_edge_ports.setdefault(dpid, set())
			# add the ports in the switch_port_table
			for port in sw.ports:
				self.switch_port_table[dpid].add(port.port_no)
		# self.logger.debug("[DEBUG][TopoDisc][create_port_map] switch_port_table %s", self.switch_port_table)

	def create_interior_links(self, links):
		"""
			It maps the link to the ports and fills self.link_to_port. and also fills the self.interior_ports.
		:param links:
		:return:
		"""
		for link in links:
			src = link.src
			dst = link.dst
			self.link_to_port[(src.dpid, dst.dpid)] = (src.port_no, dst.port_no)
			if src.dpid in self.switches:
				self.interior_ports[src.dpid].add(src.port_no)
			if dst.dpid in self.switches:
				self.interior_ports[dst.dpid].add(dst.port_no)
		self.logger.debug("[DEBUG][TopoDisc][create_interior_links] interior_ports %s", self.interior_ports)

	def create_edge_ports(self):
		"""
			Fills the switch_edge_ports
		:return:
		"""
		for sw in self.switch_port_table:
			all_port_table = self.switch_port_table[sw]
			interior_port = self.interior_ports[sw]

			self.switch_edge_ports[sw] = all_port_table - interior_port

	def create_topo_graph(self, links):
		"""
			It creates the graph of the topology (adjacency matrix).
		:param links: link_to_port
		:return: The network graph
		"""
		# _graph = self.graph.copy()
		self.graph.clear()
		for src in self.switches:
			for dst in self.switches:
				if src == dst:
					self.graph.add_edge(src, dst, weight=0)
				elif (src, dst) in links:
					self.graph.add_edge(src, dst, weight=1)
				else:
					pass
		# self.logger.info("[DEBUG][TopoDisc][create_topo_graph] graph %s", _graph)
		# return self.graph

	def get_host_location(self, host_ip):
		"""
			Returns the edge switch and port the host_ip is connected to.
		:param host_ip:
		:return: (dpid, port_no)
		"""
		access_table = self.edge_switch_table.copy()
		self.logger.info("[INFO][TopoDiscov][get_host_location()] The the access table is: %s", access_table)

		for key in access_table.keys():
			if access_table[key][0] == host_ip:
				self.logger.info("[INFO][TopoDiscov][get_host_location()] The host ip key is: (%s, %s)" % key)
				return key
		self.logger.info("%s location is not found." % host_ip)
		return None

		# for key in self.switch_port_table.keys():
		# 	# ip = self.edge_switch_table.get(key, None)[0]
		# 	self.logger.info("[INFO][TopoDiscov][get_host_location()] The host ip key is: ", key)
		# # 	if ip == host_ip:
		# # 		return key
		# # self.logger.info("[INFO][TopoDiscov][get_host_location()] The host %s location is not found.", host_ip)
		# return None

	def disply_topology(self):

		switch_num = len(list(self.graph.nodes()))
		if self.pre_graph != self.graph and DISPLAY_TOPO:
			print "---------------------Topo Link---------------------"
			print '%10s' % ("switch"),
			for i in self.graph.nodes():
				print '%10d' % i,
			print ""
			for i in self.graph.nodes():
				print '%10d' % i,
				for j in self.graph[i].values():
					print '%10.0f' % j['weight'],
				print ""
			self.pre_graph = copy.deepcopy(self.graph)

		if self.pre_link_to_port != self.link_to_port and DISPLAY_TOPO:  # if the link_to_port is changed
			_graph = self.graph.copy()
			print "\n---------------------[Link-Port]---------------------"
			print '%6s' % ('switch'),
			for node in sorted([node for node in _graph.nodes()], key=lambda node: node):
				print '%6d' % node,
			print
			for node1 in sorted([node for node in _graph.nodes()], key=lambda node: node):
				print '%6d' % node1,
				for node2 in sorted([node for node in _graph.nodes()], key=lambda node: node):
					if (node1, node2) in self.link_to_port.keys():
						print '%6s' % str(self.link_to_port[(node1, node2)]),
					else:
						print '%6s' % '/',
				print
			print
			self.pre_link_to_port = self.link_to_port.copy()

		if self.pre_edge_switch_table != self.edge_switch_table and DISPLAY_TOPO:  # edge_switch_table is changed
			print "\n----------------Access Host-------------------"
			print '%10s' % 'switch', '%10s' % 'port', '%22s' % 'Host'
			if not self.edge_switch_table.keys():
				print "No host is found."
			else:
				for sw in sorted(self.edge_switch_table.keys()):
					print '%10d' % sw[0], '%10d      ' % sw[1], self.edge_switch_table[sw]
			print
			self.pre_edge_switch_table = self.edge_switch_table.copy()
