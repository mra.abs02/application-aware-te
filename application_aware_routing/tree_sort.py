#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This code resolves the problem described in
http://xahlee.info/python/python_construct_tree_from_edge.html

by Ting-Yu Lin aethanyc 2014-01-08 https://gist.github.com/aethanyc/8313640

"""

import collections
import json


def construct_trees_by_TingYu(edges):
	"""Given a list of edges [child, parent], return trees. """
	trees = collections.defaultdict(dict)

	# it creats a tree for each parent node and for each a child key the childs referece dictionary is set as its vlaue
	for child, parent in edges:
		trees[parent][child] = trees[child]
	# 	print("parent {0} child {1}".format(parent, child))
	# 	print("trees[parent][child] = ", trees[parent][child])
	#
	# print(trees)

	# Find roots, the zip will unpack the edge and zip each two list ==> the final result is a tuple of (child nodes) and (parents nodes)
	children, parents = zip(*edges)
	roots = set(parents).difference(children)
	ans = collections.defaultdict(dict)

	print("roots ", roots)
	for root in roots:
		ans[root]= trees[root]
		print("root, ", root, ans[root])
	print ans


	return ans


if __name__ == '__main__':
	edges = [[0, 2], [3, 0], [1, 4], [2, 4]]
	print(json.dumps(construct_trees_by_TingYu(edges), indent=1))
