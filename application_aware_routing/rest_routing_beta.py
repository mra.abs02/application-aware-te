# #### TODO very important**** use 10% scratch capacity.

# TODO check if the flows and bandwidth are in kbps or mbps. line 272 in bw_mon has the for conversion.
# TODO if it could not solve the problem in a resonable time then I can create a graph from the k shortest paths for each flow. for this I need to import the host_location form the ryu app

# flow_speed with no priority, the speed is already converted.

link_to_port = {(1, 2): (1, 1), (3, 2): (2, 2), (1, 3): (2, 1), (4, 5): (2, 2), (3, 4): (3, 1), (3, 1): (1, 2), (5, 4): (2, 2), (2, 1): (1, 1), (2, 3): (2, 2), (4, 3): (1, 3), (2, 5): (3, 1), (5, 2): (1, 3)}
host_location = {(1, 3): ('10.0.0.1', '02:87:f0:32:85:d1'), (5, 3): ('10.0.0.5', 'c6:27:53:80:82:e2')}


import requests
import copy
from networkx.readwrite import json_graph
from collections import defaultdict
import ast
import numpy as np
import time
import networkx as nx
from docplex.mp.model import Model
import docplex.mp.linear


URL = "http://127.0.0.1:8080/no/mac/data"

# # TODO HARD coded the speed
sp_flow = [('10.0.0.1', '10.0.0.5')]
d = 18000

class NetworkData(object):
	def __init__(self, *args, **kwargs):
		self.edges = set()
		self.flows = set()
		self.flow_demands = {}
		self.nodes = defaultdict(dict)
		self.non_splitable_flows = set()
		self.splitable_flows = set()
		self.graph_bw = nx.DiGraph()
		self.flow_paths = {}

	def display_net_data(self):
		print("\n----------- Network Flow Data ---------\n")
		print(
			"\tedges {0}\n\tflows: {1}\n\tflow_demand: {2}\n\t flow_nodes {3}\n\tnon_splitable_flows{4}\n\tsplitable_flows {5}").format(
			self.edges, self.flows,
			self.flow_demands, self.nodes, self.non_splitable_flows, self.splitable_flows)
		print("\n------------------------------------------")


net_data = NetworkData()
model = Model('MCFP')

flow_endpoints = defaultdict(dict)
added_subpath = defaultdict(list) # to add the sub path to this dictionary for check up


def find_in_out_edges():
	"""
		Finds the in_edges and out_edges for each node in the network.
		TODO it is used for .....
	:return: None
	"""
	for e in net_data.edges:
		i = e[0]
		j = e[1]

		if i not in net_data.nodes or j not in net_data.nodes:
			print("[Error] The node of the edge %s doesn't exist." % e)
		else:
			# create a list out/in for node i
			net_data.nodes[i].setdefault('outEdges', [])
			net_data.nodes[i].setdefault('inEdges', [])

			# create a list out/in for node i
			net_data.nodes[j].setdefault('inEdges', [])
			net_data.nodes[j].setdefault('inEdges', [])

			# print("nodes_set_V[%s]: %s " %(i,nodes_set_V[i]))
			net_data.nodes[i]['outEdges'] += [e]
			net_data.nodes[j]['inEdges'] += [e]


def define_objective_function(x, y):
	"""
		Defines the objective function for the MIP model.
	:param x: continuous variables for splitable flows.
	:param y: binary variables for non-splitable flows.
	:return: None
	"""
	model.minimize(model.sum(
		(model.sum((net_data.graph_bw.get_edge_data(i, j).get('cost', 0) if net_data.graph_bw.get_edge_data(i, j) is not None else 0) * x[i, j][f] for f in net_data.splitable_flows)) +
		(model.sum((net_data.graph_bw.get_edge_data(i, j).get('cost', 0) if net_data.graph_bw.get_edge_data(i,j) is not None else 0) * y[i, j][nsf] * net_data.flow_demands[nsf] for nsf in net_data.non_splitable_flows)) for i, j in net_data.edges))

def link_capacity_constraints(x, y):
	"""
		Adds the link capacity constrains to the MIP model.
	:param x: Continuous variables for the splitable variables.
	:param y: Binary variables for the non-splitable variables.
	:return: None
	"""
	for i, j in net_data.edges:
		edge_data = net_data.graph_bw.get_edge_data(i, j)
		if edge_data is None:
			link_capacity = float('inf')
		else:
			link_capacity = edge_data['bandwidth']

		model.add_constraint(
			(((model.sum(x[i, j][f] for f in net_data.splitable_flows)) +
			  (model.sum(y[i, j][nsf] * net_data.flow_demands[nsf] for nsf in net_data.non_splitable_flows))
			  ) <= (link_capacity)), "link_constraint_(%s, %s)" % (i, j))

def flow_conservation_Splitable(x):
	"""
		Adds flow_conservation for splitable flows.
	:param: x is the splitable flows variables.
	:return: None
	"""
	for f in net_data.splitable_flows:
		source = f[0]
		target = f[1]

		for i, node_i_dic in net_data.nodes.iteritems():
			# set the flow balance, supply for each node
			# print("i ", i,"dic: ", node_i_dic)
			if i == source:
				traffic = float(net_data.flow_demands[f])
			elif i == target:
				traffic = float(-net_data.flow_demands[f])
			else:
				traffic = float(0)

			out_edges = node_i_dic.get('outEdges', [])
			in_edges = node_i_dic.get('inEdges', [])

			# for e in out_edges:
			# 	print("print e %s and %s f" %(e, f))
			# 	print("x:: ", x[e][f])
			# print("i %s and f %s"%(i, f))
			# sums = model.sum(x[e][f] for e in out_edges) - model.sum(x[e][f] for e in in_edges)
			# print sums
			model.add_constraint(
				(model.sum(x[e][f] for e in out_edges) - model.sum(x[e][f] for e in in_edges)) == traffic,
				"flow conservation for node %s, flow %s" % (i, f)
			)

def flow_conservation_non_splitable(y):
	"""
		Adds flow conservation constraints for non-splitable flows.
	:param y: Binary variable.
	:return: None
	"""
	for f in net_data.non_splitable_flows:
		source = f[0]
		target = f[1]

		for i, node_i_dic in net_data.nodes.iteritems():
			# set the flow balance, supply for each node
			# print("i ", i,"dic: ", node_i_dic)
			if i == source:
				traffic = 1
			elif i == target:
				traffic = -1
			else:
				traffic = 0

			out_edges = node_i_dic.get('outEdges', [])
			in_edges = node_i_dic.get('inEdges', [])

			# for e in out_edges:
			# 	print("print e %s and %s f" %(e, f))
			# 	print("x:: ", x[e][f])
			# print("i %s and f %s"%(i, f))
			# sums = model.sum(x[e][f] for e in out_edges) - model.sum(x[e][f] for e in in_edges)
			# print sums
			model.add_constraint(
				(model.sum(y[e][f] for e in out_edges) - model.sum(y[e][f] for e in in_edges)) == traffic,
				"Binary flow conservation for node %s, flow %s" % (i, f)
			)

def display_model_info():
	"""
		Print the information about the model.
	:return: None.
	"""
	print("------------ The objective function ------------- ")
	print(model.objective_expr)

	print("----------------- Model Info -----------------------")
	print(model.print_information())

	print("----------------- Constraints ----------------------")
	for c in model.iter_constraints():
		print("linear constraints: ", c)


def subpath_isdisjoint(p1, p2):
	if set(p1).isdisjoint(p2):
		return True


def find_dst_path(flow, curr_path, t_paths, src, dst):
	tot_path = t_paths[:]
	print("[FIND_DST_PATH] total path(in find_dst_path): ", tot_path)
	if curr_path in tot_path:
		tot_path.remove(curr_path)
	path = []
	partial_paths = []

	for p in t_paths:
		tmp_path = []
		# print("<pre> total path(in find_dst_path): ", total_paths)
		if p not in added_subpath[
			flow]:  #### TODO I think this is not necessary. I just need to check if a path has been selected befor or not
			# print("find_dst_path: curr_path: ", curr_path, "p: ", p, "t_paths: ", t_paths)
			if curr_path[-1] == p[0]:
				if subpath_isdisjoint(curr_path[:-1], p):
					if p[-1] == dst[0]:
						tmp_path = curr_path[:-1] + p
						path.append(tmp_path)  # it removes the duplicate nodes and add it to the path
						if p in tot_path:
							tot_path.remove(p)
						# added_subpath[flow].append(p) #TODO this variable is used to not over check the paths again and again
						# print("<post> total path(in find_dst_path): ", total_paths)
					else:
						tmp_path = curr_path[:-1] + p
						partial_path = find_dst_path(flow, tmp_path, t_paths, src, dst)
						print("FIND DST_PATH - REC ", partial_path)
						path.append(partial_path)
	return path

def find_source_destination_edges(flow, value_dict):
	src_edge = None
	dst_edge = None
	# print("flow ",flow)
	for _, path in value_dict.items():
		for e in path:
			if flow[0] == e[0]:
				# print("source edge", e)
				src_edge = e
			elif flow[1] == e[1]:
				# print("destination edge", e)
				dst_edge = e

	return src_edge, dst_edge

def get_path_nodes(cplex_path):
	path_nodes = defaultdict(list)
	G = nx.DiGraph()

	# now find the nodes in the paths
	for flow in cplex_path:
		src, dst = find_source_destination_edges(flow, cplex_path[flow])
		if src and dst:
			# print("flow ", flow, src, dst)
			flow_endpoints[flow]['src'] = src
			flow_endpoints[flow]['dst'] = dst
			for amount, p in cplex_path[flow].items():
				# print("amount ", amount, "path",  p)
				path = p[:]
				if src in path:
					path.remove(src)
				if dst in path:
					path.remove(dst)

				# print(path)
				if path:
					G = nx.DiGraph()
					G.add_edges_from(path)
					# dfs_nods = list(nx.dfs_preorder_nodes(G, source=src[1]))
					# dfs_nods = list(nx.dfs_preorder_nodes(G)) # ### I believe because the paths/graph is directed and there is only on root so I don't need to specify the source node
					dfs_nods = list(nx.topological_sort(G))
					# print("dfs ", dfs_nods)
					path_nodes[flow].append(dfs_nods)
					del (G)
	return path_nodes

def get_non_split_path_nodes(cplex_path):
	path_nodes = defaultdict(list)
	G = nx.DiGraph()

	# now find the nodes in the paths
	for flow, path in cplex_path.items():

		G = nx.DiGraph()
		G.add_edges_from(path)

		dfs_nods = list(nx.topological_sort(G))

		flow_endpoints[flow]['src'] = (dfs_nods[0], dfs_nods[1])
		flow_endpoints[flow]['dst'] = (dfs_nods[-2], dfs_nods[-1])
		# ### to get the path without src and dst host ip
		dfs_nods.remove(flow[0])
		dfs_nods.remove(flow[1])

		path_nodes[flow].append(dfs_nods)

		del (G)
	return path_nodes

def get_complete_path(path_nodes):
	complete_paths = defaultdict(list)

	for flow, paths in path_nodes.items():
		# TODO To use added_subpaths variable  to avoid and not over check the paths again and again
		# TODO also check if two subpaths have any nodes in common, except the end nodes (otherwise they will cause loop). use not set(a).isdisjoint(b) check this link(https://stackoverflow.com/questions/3170055/test-if-lists-share-any-items-in-python)
		src_edge = flow_endpoints[flow]['src']
		dst_edge = flow_endpoints[flow]['dst']
		total_paths = paths[:]  # this will be passed to the find_dst_paths
		for p in paths:
			# print("----------")
			# exec("if dst[0] != int(p[-1]): print('yes')"),

			# print( "src %s, p[0] %s,dst %s, p[-1] %s,p %s " %(src_edge[1], p[0],dst_edge[0],p[-1],p))
			# print("++++++++++")
			# print("(src_edge %s, src_edge[1] %s, type(src_edge[1]) %s, p[0] %s,  type(p[0]) %s, dst_edge[0] %s, type(dst_edge[0]) %s, p[-1] %s, type(p[-1]) %s, p %s)" %(src_edge, src_edge[1], type(src_edge[1]),p[0], type(p[0]), dst_edge[0],type(dst_edge[0]), p[-1], type(p[-1]), p) )
			tmp_path = []
			# print("<pre> total_path: ", total_paths)
			if src_edge[1] == p[0] and dst_edge[0] == p[-1]:

				# ### to add the src and dst host ip to the path.
				# tmp_path.append(src_edge[0])
				# tmp_path.append(dst_edge[1])
				# tmp_path = tmp_path[:1] + p + tmp_path[-1:]
				# ##
				tmp_path = p
				print("[GET COMPELETE PATH- if] in the main_for partial_path: ", tmp_path)
				# print("the path for ", flow, "is ", p, "tmp", tmp_path)
				complete_paths[flow].append(tmp_path)
				print("[GET COMPELETE PATH- if] in the main_for partial_path: ", complete_paths)

			# total_paths.remove(p)
				# print("<post> total path: ", total_paths)

			elif src_edge[1] == p[0] and dst_edge[0] != p[-1]:
				tmp_path = find_dst_path(flow, p, paths, src_edge, dst_edge)
				print("[GET COMPELETE PATH- else] in the main_for partial_path: ", tmp_path)
				complete_paths[flow].append(tmp_path)
				print("[GET COMPELETE PATH- else] in the main_for partial_path: ", complete_paths)

		# find_the_path_that_joins this path to the destination node for key k
		# else if src[1] != p[0] and dst[0] == p[-1]:
		#     # fint the path that joins this path to the source for key k
	return complete_paths

flat_list = []

def flatten_list(lst):
	print("deb l =", lst)
	for a in lst:
		print ("DEB: a= ", a)
		if(any(isinstance(i, list) for i in a)):
			el = flatten_list(a)
		else:
			return a
		flat_list.append(el)
	return flat_list


def flatten_nested_path(path):
	for k, v in path.viewitems():
		flat_list = []
		flat_path = flatten_list(v)
		print("FLATTEN_PATH ", flat_path)
		path[k] = flat_path


def convert_to_path(path):
	path_nodes = get_path_nodes(path)
	print(path_nodes)
	# a more complex path_nodes
	# path_nodes = {('10.0.0.1', '10.0.0.5'): [[1, 2, 5]], ('10.0.0.5', '10.0.0.1'): [[5, 2, 4], [4, 3, 1], [5, 4], [4, 2, 1], ]}
	complete_path = get_complete_path(path_nodes)
	flatten_path = flatten_nested_path(complete_path)
	print("complete_path: ", complete_path, "FLATTEN_LIST", flatten_path)
	return complete_path

def cplex_mode():
	"""
		Models the network for CPLEX.
	:return: None
	"""
	# TODO this for testing only. in the graph-data() I add all the flows as non_splitable flows, so I have to remove the spltable from it and add it to the spliable set
	# TODO I need to find the topological sort for the non-splitable path
	# net_data.non_splitable_flows.add(('10.0.0.5', '10.0.0.1'))
	net_data.splitable_flows.add(('10.0.0.1', '10.0.0.5'))
	net_data.non_splitable_flows.remove(('10.0.0.1', '10.0.0.5'))
	print("splitable flows %s" %net_data.splitable_flows)
	print("non-splitable flows %s" %net_data.non_splitable_flows)

	# Define the continuous variables
	x = {(i, j): {(s, t): model.continuous_var(name="x_(%s,%s)^f(%s,%s)" % (i, j, s, t)) for s, t in net_data.splitable_flows} for i, j in net_data.edges}

	# Define the binary variables
	y = {(i, j): {(s, t): model.binary_var(name="y_(%s,%s)^f(%s,%s)" % (i, j, s, t)) for s, t in net_data.non_splitable_flows} for i, j in net_data.edges}

	# Define the objective function
	define_objective_function(x, y)

	# add the link capacity constraints
	link_capacity_constraints(x, y)

	# add flow conservation constraints.
	flow_conservation_Splitable(x)
	flow_conservation_non_splitable(y)

	# display the model information
	# display_model_info()

	# Solve the model
	solution = model.solve(log_output=True)
	print("----------------- solution ---------------")
	# print(solution)
	print("--------------------------------------------")

	splitable_flows_paths = defaultdict(lambda: defaultdict(list))
	non_splitable_flows_paths = defaultdict(list)
	edge_flow_amount = defaultdict(lambda: defaultdict(dict))

	# getting the splitable flows on the edges
	for edge in x.keys():
		# print(type(edge))
		for flow in x[edge].keys():
			# print(x[edge][flow])
	# 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key]._get_solution_value()))
	# 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key].get_name()))
	# 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key].solution_value))
			sol_value = x[edge][flow].solution_value
			if sol_value is 0:
				continue
			else:
				edge_flow_amount[flow][edge[0]][edge[1]] = sol_value  # to store the flow amount on each edge
				splitable_flows_paths[flow][sol_value].append(edge)
	print
	print(splitable_flows_paths)
	splitable_paths = convert_to_path(splitable_flows_paths)
	# print("split_path_nodes: ", convert_to_path(splitable_flows_paths))
	print
	# getting the non_splitable flows on the edges
	for edge in y.keys():
		# print(type(edge))
		for flow in y[edge].keys():
			# print(x[edge][flow])
			# ### Different ways of getting solution values
	 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key]._get_solution_value()))
	 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key].get_name()))
	 		# print("flow {0}, value: {1}".format(x[k][key], x[k][key].solution_value))
			sol_value = y[edge][flow].solution_value
			if sol_value is 0:
				continue
			else:
				edge_flow_amount[flow][edge[0]][edge[1]] = sol_value  # There is no need to get the value for non_split flows
				# as the whole flow should follow on route. But I used it for backward compatibality.
				non_splitable_flows_paths[flow].append(edge)

	print("non-split ", non_splitable_flows_paths)
	# print("path nodes:  ", get_non_split_path_nodes(non_splitable_flows_paths))
	non_split_paths = get_non_split_path_nodes(non_splitable_flows_paths)
	all_flows_paths = {}
	all_flows_paths['splitable'] = str(dict(splitable_paths))
	all_flows_paths['non_split'] = str(dict(non_split_paths))
	tmp = {}
	for k, v in edge_flow_amount.viewitems():
		tmp[k] = dict(v)
	all_flows_paths["amount"] = str(tmp)

	# print("the amound of flow for each edge: ", edge_flow_amount)
	# returning all the calculated paths
	return all_flows_paths
	# print("all_flows_paths: ", all_flows_paths)


	# print("flow_endpoints: ", flow_endpoints)
	# get_path_nodes(non_splitable_flows_paths)

			# print(model.get_var_by_name(str(x[k][key])))
	# print("%s, %s", net_data.graph_bw[1][2]['cost'])
	# print("%s, %s", net_data.graph_bw[2][5]['cost'])
	# print("x = %s", x)
	# print("x = %s", y)

	# d = {(1, 2): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 9},
	# 	 (3, 2): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (1, 3): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 4},
	# 	 (1, u'10.0.0.1'): {
	# 		 ('10.0.0.5', '10.0.0.1'): 2,
	# 		 ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (4, 5): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 4},
	# 	 (3, 4): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 4},
	# 	 (3, 1): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (u'10.0.0.5', 5): {
	# 		 ('10.0.0.5', '10.0.0.1'): 2,
	# 		 ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (5, 4): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (2, 1): {('10.0.0.5', '10.0.0.1'): 2,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (2, 3): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (5, u'10.0.0.5'): {
	# 		 ('10.0.0.5', '10.0.0.1'): 0,
	# 		 ('10.0.0.1', '10.0.0.5'): 9},
	# 	 (4, 3): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 0},
	# 	 (u'10.0.0.1', 1): {
	# 		 ('10.0.0.5', '10.0.0.1'): 0,
	# 		 ('10.0.0.1', '10.0.0.5'): 9},
	# 	 (2, 5): {('10.0.0.5', '10.0.0.1'): 0,
	# 			  ('10.0.0.1', '10.0.0.5'): 5},
	# 	 (5, 2): {('10.0.0.5', '10.0.0.1'): 2,
	# 			  ('10.0.0.1', '10.0.0.5'): 0}}
	#
	# G1 = nx.from_dict_of_dicts(d)

def request_get(url):
	try:
		req_get = requests.get(url)

		if req_get.status_code != 200:
			print("GET error: {}".format(req_get.status_code))
			exit()

	except:
		print("cannot connect to the server, sleeping...")
		exit()

	network = req_get.json()
	print
	print("THE NETWORK DATA: ", network)
	print
	# network = {u'new_flows': {u"('10.0.0.1', '10.0.0.5')": {u'path': [1, 2, 5]}, u"('10.0.0.2', '10.0.0.1')": {u'path': [2, 1], u'type': u'S'}, u"('10.0.0.4', '10.0.0.5')": {u'path': [4, 5]}, u"('10.0.0.2', '10.0.0.5')": {u'path': [2, 5]}, u"('10.0.0.4', '10.0.0.2')": {u'path': [4, 2], u'type': u'S'}, u"('10.0.0.1', '10.0.0.4')": {u'path': [1, 2, 4]}, u"('10.0.0.5', '10.0.0.2')": {u'path': [5, 2], u'type': u'S'}, u"('10.0.0.1', '10.0.0.2')": {u'path': [1, 2]}, u"('10.0.0.3', '10.0.0.1')": {u'path': [3, 1], u'type': u'S'}, u"('10.0.0.4', '10.0.0.3')": {u'path': [4, 3], u'type': u'S'}, u"('10.0.0.5', '10.0.0.1')": {u'path': [5, 2, 1], u'type': u'S'}, u"('10.0.0.4', '10.0.0.1')": {u'path': [4, 2, 1], u'type': u'S'}, u"('10.0.0.1', '10.0.0.3')": {u'path': [1, 3]}, u"('10.0.0.5', '10.0.0.4')": {u'path': [5, 4], u'type': u'S'}, u"('10.0.0.2', '10.0.0.3')": {u'path': [2, 3]}, u"('10.0.0.2', '10.0.0.4')": {u'path': [2, 4]}, u"('10.0.0.5', '10.0.0.3')": {u'path': [5, 2, 3], u'type': u'S'}, u"('10.0.0.3', '10.0.0.2')": {u'path': [3, 2], u'type': u'S'}, u"('10.0.0.3', '10.0.0.5')": {u'path': [3, 2, 5]}, u"('10.0.0.3', '10.0.0.4')": {u'path': [3, 4]}}, u'flow_speed_priority': {u'1': {u"(1, '10.0.0.1', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.5')": [1243818.1818181826, 1273090.636704119, 1307064.2018486136, 1209419.4354234324, 1273503.9960039968], u"(1, '10.0.0.3', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.1')": [27164.835164835178, 26432.95880149811, 29431.926055458407, 50477.142143392455, 61273.72627372631]}, u'3': {u"(1, '10.0.0.3', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.3', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.3', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.3', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0]}, u'2': {u"(1, '10.0.0.2', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.1')": [27181.318681318648, 26403.294235088608, 29462.768615692188, 50441.05894105888, 61281.89762796511], u"(1, '10.0.0.3', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.5')": [1213357.1428571413, 1205138.0084851517, 1209176.4117941044, 1215465.534465533, 1221674.4069912622], u"(1, '10.0.0.3', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0]}, u'5': {u"(1, '10.0.0.3', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.5')": [1211877.622377621, 1206178.2326510255, 1208436.2818590698, 1214725.774225775, 1222397.5031210978], u"(1, '10.0.0.5', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.1')": [27181.318681318648, 26426.36045931108, 29446.276861569197, 50441.05894105897, 61281.897627965]}, u'4': {u"(1, '10.0.0.2', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.3', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0]}}, u'graph': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}, {u'id': u'10.0.0.5'}, {u'id': u'10.0.0.4'}, {u'id': u'10.0.0.1'}, {u'id': u'10.0.0.3'}, {u'id': u'10.0.0.2'}], u'links': [{u'source': 1, u'target': 1, u'weight': 0}, {u'source': 1, u'target': 2, u'weight': 1}, {u'source': 1, u'target': 3, u'weight': 1}, {u'source': 1, u'mac_add': u'f6:78:11:f8:cb:54', u'weight': 1, u'dpid_port': 3, u'target': u'10.0.0.1'}, {u'source': 2, u'target': 1, u'weight': 1}, {u'source': 2, u'target': 2, u'weight': 0}, {u'source': 2, u'target': 3, u'weight': 1}, {u'source': 2, u'target': 4, u'weight': 1}, {u'source': 2, u'target': 5, u'weight': 1}, {u'source': 2, u'mac_add': u'f2:9a:22:9b:d7:3a', u'weight': 1, u'dpid_port': 5, u'target': u'10.0.0.2'}, {u'source': 3, u'target': 1, u'weight': 1}, {u'source': 3, u'target': 2, u'weight': 1}, {u'source': 3, u'target': 3, u'weight': 0}, {u'source': 3, u'target': 4, u'weight': 1}, {u'source': 3, u'mac_add': u'b2:96:18:1b:74:5d', u'weight': 1, u'dpid_port': 4, u'target': u'10.0.0.3'}, {u'source': 4, u'mac_add': u'e2:46:64:2d:06:d3', u'weight': 1, u'dpid_port': 4, u'target': u'10.0.0.4'}, {u'source': 4, u'target': 2, u'weight': 1}, {u'source': 4, u'target': 3, u'weight': 1}, {u'source': 4, u'target': 4, u'weight': 0}, {u'source': 4, u'target': 5, u'weight': 1}, {u'source': 5, u'mac_add': u'a6:2f:f3:99:60:b2', u'weight': 1, u'dpid_port': 3, u'target': u'10.0.0.5'}, {u'source': 5, u'target': 2, u'weight': 1}, {u'source': 5, u'target': 4, u'weight': 1}, {u'source': 5, u'target': 5, u'weight': 0}, {u'source': u'10.0.0.5', u'mac_add': u'a6:2f:f3:99:60:b2', u'weight': 1, u'dpid_port': 3, u'target': 5}, {u'source': u'10.0.0.4', u'mac_add': u'e2:46:64:2d:06:d3', u'weight': 1, u'dpid_port': 4, u'target': 4}, {u'source': u'10.0.0.1', u'mac_add': u'f6:78:11:f8:cb:54', u'weight': 1, u'dpid_port': 3, u'target': 1}, {u'source': u'10.0.0.3', u'mac_add': u'b2:96:18:1b:74:5d', u'weight': 1, u'dpid_port': 4, u'target': 3}, {u'source': u'10.0.0.2', u'mac_add': u'f2:9a:22:9b:d7:3a', u'weight': 1, u'dpid_port': 5, u'target': 2}], u'multigraph': False}, u'flow_speed': {u'1': {u"('10.0.0.1', '10.0.0.5')": [9950.545454545461, 10184.725093632951, 10456.513614788908, 9675.355483387459, 10188.031968031975], u"('10.0.0.2', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.4')": [0.2613333333333333], u"('10.0.0.1', '10.0.0.2')": [0.2613333333333333], u"('10.0.0.3', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.3')": [0.2613333333333333], u"('10.0.0.5', '10.0.0.1')": [217.31868131868143, 211.46367041198488, 235.45540844366727, 403.81713714713965, 490.1898101898105]}, u'3': {u"('10.0.0.3', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.2', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.5')": [0.2613333333333333], u"('10.0.0.3', '10.0.0.4')": [0.2613333333333333]}, u'2': {u"('10.0.0.1', '10.0.0.5')": [9706.85714285713, 9641.104067881213, 9673.411294352834, 9723.724275724264, 9773.395255930098], u"('10.0.0.2', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.2', '10.0.0.4')": [0.2613333333333333], u"('10.0.0.4', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.2', '10.0.0.3')": [0.2613333333333333], u"('10.0.0.2', '10.0.0.5')": [0.2613333333333333], u"('10.0.0.3', '10.0.0.5')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.1')": [217.45054945054918, 211.22635388070887, 235.7021489255375, 403.528471528471, 490.25518102372087], u"('10.0.0.5', '10.0.0.2')": [0.5226666666666666]}, u'5': {u"('10.0.0.1', '10.0.0.5')": [9695.020979020968, 9649.425861208205, 9667.490254872559, 9717.8061938062, 9779.180024968782], u"('10.0.0.4', '10.0.0.5')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.5')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.1')": [217.45054945054918, 211.41088367448864, 235.57021489255357, 403.52847152847175, 490.25518102372], u"('10.0.0.2', '10.0.0.5')": [0.5226666666666666]}, u'4': {u"('10.0.0.4', '10.0.0.5')": [0.2613333333333333], u"('10.0.0.4', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.2', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.4')": [0.5226666666666666]}}, u'delay': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}], u'links': [{u'delay': 0, u'source': 1, u'target': 1, u'weight': 0}, {u'lldpDelay': 0.48047590255737305, u'delay': 0.45553088188171387, u'target': 2, u'weight': 1, u'source': 1}, {u'lldpDelay': 0.004106998443603516, u'delay': 0.0009685754776000977, u'target': 3, u'weight': 1, u'source': 1}, {u'lldpDelay': 0.002650022506713867, u'delay': 0.0018768310546875, u'target': 1, u'weight': 1, u'source': 2}, {u'delay': 0, u'source': 2, u'target': 2, u'weight': 0}, {u'lldpDelay': 0.002546072006225586, u'delay': 0.0022214651107788086, u'target': 3, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.0037088394165039062, u'delay': 0.0027159452438354492, u'target': 4, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.01230001449584961, u'delay': 0.008968830108642578, u'target': 5, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.0026619434356689453, u'delay': 0.0015984773635864258, u'target': 1, u'weight': 1, u'source': 3}, {u'lldpDelay': 0.0028111934661865234, u'delay': 0.0022584199905395508, u'target': 2, u'weight': 1, u'source': 3}, {u'delay': 0, u'source': 3, u'target': 3, u'weight': 0}, {u'lldpDelay': 0.0022020339965820312, u'delay': 0.0020835399627685547, u'target': 4, u'weight': 1, u'source': 3}, {u'lldpDelay': 0.002856016159057617, u'delay': 0.003210902214050293, u'target': 2, u'weight': 1, u'source': 4}, {u'lldpDelay': 0.002101898193359375, u'delay': 0.0021305084228515625, u'target': 3, u'weight': 1, u'source': 4}, {u'delay': 0, u'source': 4, u'target': 4, u'weight': 0}, {u'lldpDelay': 0.0025780200958251953, u'delay': 0.0018409490585327148, u'target': 5, u'weight': 1, u'source': 4}, {u'lldpDelay': 0.0025529861450195312, u'delay': 0.0016460418701171875, u'target': 2, u'weight': 1, u'source': 5}, {u'lldpDelay': 0.0026051998138427734, u'delay': 0.0013860464096069336, u'target': 4, u'weight': 1, u'source': 5}, {u'delay': 0, u'source': 5, u'target': 5, u'weight': 0}], u'multigraph': False}, u'bw': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}], u'links': [{u'source': 1, u'target': 1, u'weight': 0}, {u'bw_usage': 10000, u'target': 2, u'weight': 1, u'util_ratio': 1.0, u'source': 1, u'bandwidth': 0, u'cost': 500, u'congested': True}, {u'bw_usage': 0.9590409590400668, u'target': 3, u'weight': 1, u'util_ratio': 9.590409590400668e-05, u'source': 1, u'bandwidth': 9999.04095904096, u'cost': 1, u'congested': False}, {u'bw_usage': 10000, u'target': 1, u'weight': 1, u'util_ratio': 1.0, u'source': 2, u'bandwidth': 0, u'cost': 500, u'congested': True}, {u'source': 2, u'target': 2, u'weight': 0}, {u'bw_usage': 1.0789210789207573, u'target': 3, u'weight': 1, u'util_ratio': 0.00010789210789207573, u'source': 2, u'bandwidth': 9998.92107892108, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9592805395950563, u'target': 4, u'weight': 1, u'util_ratio': 9.592805395950563e-05, u'source': 2, u'bandwidth': 9999.040719460405, u'cost': 1, u'congested': False}, {u'bw_usage': 10000, u'target': 5, u'weight': 1, u'util_ratio': 1.0, u'source': 2, u'bandwidth': 0, u'cost': 500, u'congested': True}, {u'bw_usage': 0.9590409590400668, u'target': 1, u'weight': 1, u'util_ratio': 9.590409590400668e-05, u'source': 3, u'bandwidth': 9999.04095904096, u'cost': 1, u'congested': False}, {u'bw_usage': 1.0789210789207573, u'target': 2, u'weight': 1, u'util_ratio': 0.00010789210789207573, u'source': 3, u'bandwidth': 9998.92107892108, u'cost': 1, u'congested': False}, {u'source': 3, u'target': 3, u'weight': 0}, {u'bw_usage': 1.0791906070444384, u'target': 4, u'weight': 1, u'util_ratio': 0.00010791906070444384, u'source': 3, u'bandwidth': 9998.920809392956, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9592805395950563, u'target': 2, u'weight': 1, u'util_ratio': 9.592805395950563e-05, u'source': 4, u'bandwidth': 9999.040719460405, u'cost': 1, u'congested': False}, {u'bw_usage': 1.0791906070444384, u'target': 3, u'weight': 1, u'util_ratio': 0.00010791906070444384, u'source': 4, u'bandwidth': 9998.920809392956, u'cost': 1, u'congested': False}, {u'source': 4, u'target': 4, u'weight': 0}, {u'bw_usage': 1.0791906070444384, u'target': 5, u'weight': 1, u'util_ratio': 0.00010791906070444384, u'source': 4, u'bandwidth': 9998.920809392956, u'cost': 1, u'congested': False}, {u'bw_usage': 10000, u'target': 2, u'weight': 1, u'util_ratio': 1.0, u'source': 5, u'bandwidth': 0, u'cost': 500, u'congested': True}, {u'bw_usage': 1.0791906070444384, u'target': 4, u'weight': 1, u'util_ratio': 0.00010791906070444384, u'source': 5, u'bandwidth': 9998.920809392956, u'cost': 1, u'congested': False}, {u'source': 5, u'target': 5, u'weight': 0}], u'multigraph': False}}

	# network = {u'new_flows': {u"('10.0.0.1', '10.0.0.5')": {u'path': [2, 5], u'type': u'S'}, u"('10.0.0.2', '10.0.0.1')": {u'path': [2, 1], u'type': u'S'}, u"('10.0.0.4', '10.0.0.5')": {u'path': [4, 5]}, u"('10.0.0.2', '10.0.0.5')": {u'path': [2, 5]}, u"('10.0.0.4', '10.0.0.2')": {u'path': [4, 2], u'type': u'S'}, u"('10.0.0.1', '10.0.0.4')": {u'path': [1, 2, 4]}, u"('10.0.0.5', '10.0.0.2')": {u'path': [5, 2], u'type': u'S'}, u"('10.0.0.1', '10.0.0.2')": {u'path': [1, 2]}, u"('10.0.0.3', '10.0.0.1')": {u'path': [3, 1], u'type': u'S'}, u"('10.0.0.4', '10.0.0.3')": {u'path': [4, 3], u'type': u'S'}, u"('10.0.0.5', '10.0.0.1')": {u'path': [5, 2], u'type': u'S'}, u"('10.0.0.4', '10.0.0.1')": {u'path': [4, 2, 1], u'type': u'S'}, u"('10.0.0.1', '10.0.0.3')": {u'path': [1, 3]}, u"('10.0.0.5', '10.0.0.4')": {u'path': [5, 4], u'type': u'S'}, u"('10.0.0.2', '10.0.0.3')": {u'path': [2, 3]}, u"('10.0.0.2', '10.0.0.4')": {u'path': [2, 4]}, u"('10.0.0.5', '10.0.0.3')": {u'path': [5, 2, 3], u'type': u'S'}, u"('10.0.0.3', '10.0.0.2')": {u'path': [3, 2], u'type': u'S'}, u"('10.0.0.3', '10.0.0.5')": {u'path': [3, 2, 5]}, u"('10.0.0.3', '10.0.0.4')": {u'path': [3, 4]}}, u'flow_speed_priority': {u'1': {u"(1, '10.0.0.1', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.5')": [1247828.6285286036, 1416199.40029985, 1564876.3427429427, 1259534.7326336836, 1181573.0337078655], u"(1, '10.0.0.3', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.1')": [26726.455158631026, 26584.707646176907, 28321.25905570822, 50848.07596201901, 54445.94257178528]}, u'3': {u"(1, '10.0.0.3', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.3', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.3', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.3', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0]}, u'2': {u"(1, '10.0.0.2', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.1')": [26733.133433283365, 26594.554084436673, 28311.84407796103, 50871.56421789102, 54425.3619570644], u"(1, '10.0.0.3', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.5')": [1212630.6846576715, 1211521.8586060454, 1213617.1914042982, 1214297.851074462, 1218194.7079380928], u"(1, '10.0.0.3', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0]}, u'5': {u"(1, '10.0.0.3', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.5')": [1212705.9705221085, 1209225.887056472, 1213314.0144891331, 1214107.4731317167, 1219788.8167748377], u"(1, '10.0.0.5', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.2', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.1')": [26742.94279290532, 26568.21589205398, 28304.771421433925, 50907.7730567358, 54401.89715426859]}, u'4': {u"(1, '10.0.0.2', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.5')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.1', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.5', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.2')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.1')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.3', '10.0.0.4')": [0.0, 0.0, 0.0, 0.0, 0.0], u"(1, '10.0.0.4', '10.0.0.3')": [0.0, 0.0, 0.0, 0.0, 0.0]}}, u'graph': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}, {u'id': u'10.0.0.5'}, {u'id': u'10.0.0.4'}, {u'id': u'10.0.0.1'}, {u'id': u'10.0.0.3'}, {u'id': u'10.0.0.2'}], u'links': [{u'source': 1, u'target': 1, u'weight': 0}, {u'source': 1, u'target': 2, u'weight': 1}, {u'source': 1, u'target': 3, u'weight': 1}, {u'source': 1, u'mac_add': u'de:f0:ad:59:a6:46', u'weight': 1, u'dpid_port': 3, u'target': u'10.0.0.1'}, {u'source': 2, u'target': 1, u'weight': 1}, {u'source': 2, u'target': 2, u'weight': 0}, {u'source': 2, u'target': 3, u'weight': 1}, {u'source': 2, u'target': 4, u'weight': 1}, {u'source': 2, u'target': 5, u'weight': 1}, {u'source': 2, u'mac_add': u'56:a8:55:75:85:63', u'weight': 1, u'dpid_port': 5, u'target': u'10.0.0.2'}, {u'source': 3, u'target': 1, u'weight': 1}, {u'source': 3, u'target': 2, u'weight': 1}, {u'source': 3, u'target': 3, u'weight': 0}, {u'source': 3, u'target': 4, u'weight': 1}, {u'source': 3, u'mac_add': u'72:e4:b5:b4:64:06', u'weight': 1, u'dpid_port': 4, u'target': u'10.0.0.3'}, {u'source': 4, u'mac_add': u'b6:c8:07:21:96:ae', u'weight': 1, u'dpid_port': 4, u'target': u'10.0.0.4'}, {u'source': 4, u'target': 2, u'weight': 1}, {u'source': 4, u'target': 3, u'weight': 1}, {u'source': 4, u'target': 4, u'weight': 0}, {u'source': 4, u'target': 5, u'weight': 1}, {u'source': 5, u'mac_add': u'0e:d8:90:91:13:d8', u'weight': 1, u'dpid_port': 3, u'target': u'10.0.0.5'}, {u'source': 5, u'target': 2, u'weight': 1}, {u'source': 5, u'target': 4, u'weight': 1}, {u'source': 5, u'target': 5, u'weight': 0}, {u'source': u'10.0.0.5', u'mac_add': u'0e:d8:90:91:13:d8', u'weight': 1, u'dpid_port': 3, u'target': 5}, {u'source': u'10.0.0.4', u'mac_add': u'b6:c8:07:21:96:ae', u'weight': 1, u'dpid_port': 4, u'target': 4}, {u'source': u'10.0.0.1', u'mac_add': u'de:f0:ad:59:a6:46', u'weight': 1, u'dpid_port': 3, u'target': 1}, {u'source': u'10.0.0.3', u'mac_add': u'72:e4:b5:b4:64:06', u'weight': 1, u'dpid_port': 4, u'target': 3}, {u'source': u'10.0.0.2', u'mac_add': u'56:a8:55:75:85:63', u'weight': 1, u'dpid_port': 5, u'target': 2}], u'multigraph': False}, u'flow_speed': {u'1': {u"('10.0.0.1', '10.0.0.5')": [9982.629028228828, 11329.5952023988, 12519.010741943543, 10076.27786106947, 9452.584269662924], u"('10.0.0.2', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.4')": [0.2613333333333333], u"('10.0.0.1', '10.0.0.2')": [0.2613333333333333], u"('10.0.0.3', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.3')": [0.2613333333333333], u"('10.0.0.5', '10.0.0.1')": [213.8116412690482, 212.67766116941525, 226.57007244566574, 406.7846076961521, 435.56754057428225]}, u'3': {u"('10.0.0.3', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.2', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.5')": [0.2613333333333333], u"('10.0.0.3', '10.0.0.4')": [0.2613333333333333]}, u'2': {u"('10.0.0.1', '10.0.0.5')": [9701.045477261372, 9692.174868848362, 9708.937531234385, 9714.382808595696, 9745.557663504742], u"('10.0.0.2', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.2', '10.0.0.4')": [0.2613333333333333], u"('10.0.0.4', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.2', '10.0.0.3')": [0.2613333333333333], u"('10.0.0.2', '10.0.0.5')": [0.2613333333333333], u"('10.0.0.3', '10.0.0.5')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.1')": [213.8650674662669, 212.7564326754934, 226.4947526236882, 406.97251374312816, 435.4028956565152], u"('10.0.0.5', '10.0.0.2')": [0.5226666666666666]}, u'5': {u"('10.0.0.1', '10.0.0.5')": [9701.647764176869, 9673.807096451776, 9706.512115913065, 9712.859785053734, 9758.310534198701], u"('10.0.0.4', '10.0.0.5')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.5')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.1')": [213.94354234324257, 212.54572713643185, 226.4381713714714, 407.2621844538864, 435.2151772341487], u"('10.0.0.2', '10.0.0.5')": [0.5226666666666666]}, u'4': {u"('10.0.0.4', '10.0.0.5')": [0.2613333333333333], u"('10.0.0.4', '10.0.0.2')": [0.5226666666666666], u"('10.0.0.1', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.3')": [0.5226666666666666], u"('10.0.0.4', '10.0.0.1')": [0.5226666666666666], u"('10.0.0.5', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.2', '10.0.0.4')": [0.5226666666666666], u"('10.0.0.3', '10.0.0.4')": [0.5226666666666666]}}, u'delay': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}], u'links': [{u'delay': 0, u'source': 1, u'target': 1, u'weight': 0}, {u'lldpDelay': 0.3482789993286133, u'delay': 0.22562050819396973, u'target': 2, u'weight': 1, u'source': 1}, {u'lldpDelay': 0.002794981002807617, u'delay': 0.0012736320495605469, u'target': 3, u'weight': 1, u'source': 1}, {u'lldpDelay': 0.0025949478149414062, u'delay': 0.0009906291961669922, u'target': 1, u'weight': 1, u'source': 2}, {u'delay': 0, u'source': 2, u'target': 2, u'weight': 0}, {u'lldpDelay': 0.002992868423461914, u'delay': 0.0015799999237060547, u'target': 3, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.0036530494689941406, u'delay': 0.0024678707122802734, u'target': 4, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.04188394546508789, u'delay': 0.044760942459106445, u'target': 5, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.003281116485595703, u'delay': 0.0017132759094238281, u'target': 1, u'weight': 1, u'source': 3}, {u'lldpDelay': 0.002444028854370117, u'delay': 0.0015888214111328125, u'target': 2, u'weight': 1, u'source': 3}, {u'delay': 0, u'source': 3, u'target': 3, u'weight': 0}, {u'lldpDelay': 0.0018100738525390625, u'delay': 0.0017218589782714844, u'target': 4, u'weight': 1, u'source': 3}, {u'lldpDelay': 0.0027968883514404297, u'delay': 0.009006977081298828, u'target': 2, u'weight': 1, u'source': 4}, {u'lldpDelay': 0.0018999576568603516, u'delay': 0.0016117095947265625, u'target': 3, u'weight': 1, u'source': 4}, {u'delay': 0, u'source': 4, u'target': 4, u'weight': 0}, {u'lldpDelay': 0.0017879009246826172, u'delay': 0.0013489723205566406, u'target': 5, u'weight': 1, u'source': 4}, {u'lldpDelay': 0.0025000572204589844, u'delay': 0.001744985580444336, u'target': 2, u'weight': 1, u'source': 5}, {u'lldpDelay': 0.0018329620361328125, u'delay': 0.001470804214477539, u'target': 4, u'weight': 1, u'source': 5}, {u'delay': 0, u'source': 5, u'target': 5, u'weight': 0}], u'multigraph': False}, u'bw': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}], u'links': [{u'source': 1, u'target': 1, u'weight': 0}, {u'bw_usage': 10000, u'target': 2, u'weight': 1, u'util_ratio': 1.0, u'source': 1, u'bandwidth': 0, u'cost': 500, u'congested': True}, {u'bw_usage': 1.0794602698642848, u'target': 3, u'weight': 1, u'util_ratio': 0.00010794602698642847, u'source': 1, u'bandwidth': 9998.920539730136, u'cost': 1, u'congested': False}, {u'bw_usage': 10000, u'target': 1, u'weight': 1, u'util_ratio': 1.0, u'source': 2, u'bandwidth': 0, u'cost': 500, u'congested': True}, {u'source': 2, u'target': 2, u'weight': 0}, {u'bw_usage': 1.0794602698642848, u'target': 3, u'weight': 1, u'util_ratio': 0.00010794602698642847, u'source': 2, u'bandwidth': 9998.920539730136, u'cost': 1, u'congested': False}, {u'bw_usage': 1.0794602698642848, u'target': 4, u'weight': 1, u'util_ratio': 0.00010794602698642847, u'source': 2, u'bandwidth': 9998.920539730136, u'cost': 1, u'congested': False}, {u'bw_usage': 10000, u'target': 5, u'weight': 1, u'util_ratio': 1.0, u'source': 2, u'bandwidth': 0, u'cost': 500, u'congested': True}, {u'bw_usage': 1.0794602698642848, u'target': 1, u'weight': 1, u'util_ratio': 0.00010794602698642847, u'source': 3, u'bandwidth': 9998.920539730136, u'cost': 1, u'congested': False}, {u'bw_usage': 1.0794602698642848, u'target': 2, u'weight': 1, u'util_ratio': 0.00010794602698642847, u'source': 3, u'bandwidth': 9998.920539730136, u'cost': 1, u'congested': False}, {u'source': 3, u'target': 3, u'weight': 0}, {u'bw_usage': 0.9595202398795664, u'target': 4, u'weight': 1, u'util_ratio': 9.595202398795663e-05, u'source': 3, u'bandwidth': 9999.04047976012, u'cost': 1, u'congested': False}, {u'bw_usage': 1.0794602698642848, u'target': 2, u'weight': 1, u'util_ratio': 0.00010794602698642847, u'source': 4, u'bandwidth': 9998.920539730136, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9595202398795664, u'target': 3, u'weight': 1, u'util_ratio': 9.595202398795663e-05, u'source': 4, u'bandwidth': 9999.04047976012, u'cost': 1, u'congested': False}, {u'source': 4, u'target': 4, u'weight': 0}, {u'bw_usage': 0.9597600599845464, u'target': 5, u'weight': 1, u'util_ratio': 9.597600599845464e-05, u'source': 4, u'bandwidth': 9999.040239940015, u'cost': 1, u'congested': False}, {u'bw_usage': 10000, u'target': 2, u'weight': 1, u'util_ratio': 1.0, u'source': 5, u'bandwidth': 0, u'cost': 500, u'congested': True}, {u'bw_usage': 0.9597600599845464, u'target': 4, u'weight': 1, u'util_ratio': 9.597600599845464e-05, u'source': 5, u'bandwidth': 9999.040239940015, u'cost': 1, u'congested': False}, {u'source': 5, u'target': 5, u'weight': 0}], u'multigraph': False}}


	get_graph_data(network)
	find_in_out_edges()

	# returning all the paths
	return cplex_mode()
	# network = {u'new_flows': {u"('10.0.0.1', '10.0.0.5')": {u'path': [1, 2, 5], u'type': u'S'}}, u'flow_speed_priority': {u'1': {u"(1, '10.0.0.1', '10.0.0.5')": [1200971.0], u"(1, '10.0.0.5', '10.0.0.1')": [23368.0]}, u'3': {}, u'2': {u"(1, '10.0.0.1', '10.0.0.5')": [1100991.0], u"(1, '10.0.0.5', '10.0.0.1')": [23401.0]}, u'5': {u"(1, '10.0.0.1', '10.0.0.5')": [1099510.0], u"(1, '10.0.0.5', '10.0.0.1')": [23401.0]}, u'4': {}}, u'graph': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}, {u'id': u'10.0.0.5'}, {u'id': u'10.0.0.1'}], u'links': [{u'source': 0, u'target': 0, u'weight': 0}, {u'source': 0, u'target': 1, u'weight': 1}, {u'source': 0, u'target': 2, u'weight': 1}, {u'source': 0, u'mac_add': u'9e:33:34:2a:9a:64', u'weight': 1, u'dpid_port': 3, u'target': 6}, {u'source': 1, u'target': 0, u'weight': 1}, {u'source': 1, u'target': 1, u'weight': 0}, {u'source': 1, u'target': 2, u'weight': 1}, {u'source': 1, u'target': 4, u'weight': 1}, {u'source': 2, u'target': 0, u'weight': 1}, {u'source': 2, u'target': 1, u'weight': 1}, {u'source': 2, u'target': 2, u'weight': 0}, {u'source': 2, u'target': 3, u'weight': 1}, {u'source': 3, u'target': 2, u'weight': 1}, {u'source': 3, u'target': 3, u'weight': 0}, {u'source': 3, u'target': 4, u'weight': 1}, {u'source': 4, u'mac_add': u'd2:8e:a4:c8:f4:94', u'weight': 1, u'dpid_port': 3, u'target': 5}, {u'source': 4, u'target': 1, u'weight': 1}, {u'source': 4, u'target': 3, u'weight': 1}, {u'source': 4, u'target': 4, u'weight': 0}, {u'source': 5, u'mac_add': u'd2:8e:a4:c8:f4:94', u'weight': 1, u'dpid_port': 3, u'target': 4}, {u'source': 6, u'mac_add': u'9e:33:34:2a:9a:64', u'weight': 1, u'dpid_port': 3, u'target': 0}], u'multigraph': False}, u'flow_speed': {u'1': {u"('10.0.0.1', '10.0.0.5')": [9607.768], u"('10.0.0.5', '10.0.0.1')": [186.944]}, u'3': {}, u'2': {u"('10.0.0.1', '10.0.0.5')": [8807.928], u"('10.0.0.5', '10.0.0.1')": [187.208]}, u'5': {u"('10.0.0.1', '10.0.0.5')": [8796.08], u"('10.0.0.5', '10.0.0.1')": [187.208]}, u'4': {}}, u'delay': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}], u'links': [{u'delay': 0, u'source': 0, u'target': 0, u'weight': 0}, {u'lldpDelay': 0.17779088020324707, u'delay': 0.1767369508743286, u'target': 1, u'weight': 1, u'source': 0}, {u'lldpDelay': 0.0030040740966796875, u'delay': 0.0013120174407958984, u'target': 2, u'weight': 1, u'source': 0}, {u'lldpDelay': 0.0029020309448242188, u'delay': 0.0018461942672729492, u'target': 0, u'weight': 1, u'source': 1}, {u'delay': 0, u'source': 1, u'target': 1, u'weight': 0}, {u'lldpDelay': 0.0025911331176757812, u'delay': 0.001723170280456543, u'target': 2, u'weight': 1, u'source': 1}, {u'lldpDelay': 0.004682064056396484, u'delay': 0.004734992980957031, u'target': 4, u'weight': 1, u'source': 1}, {u'lldpDelay': 0.0023500919342041016, u'delay': 0.0012731552124023438, u'target': 0, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.002621889114379883, u'delay': 0.0050460100173950195, u'target': 1, u'weight': 1, u'source': 2}, {u'delay': 0, u'source': 2, u'target': 2, u'weight': 0}, {u'lldpDelay': 0.0025818347930908203, u'delay': 0.001538395881652832, u'target': 3, u'weight': 1, u'source': 2}, {u'lldpDelay': 0.002341032028198242, u'delay': 0.0013605356216430664, u'target': 2, u'weight': 1, u'source': 3}, {u'delay': 0, u'source': 3, u'target': 3, u'weight': 0}, {u'lldpDelay': 0.0031681060791015625, u'delay': 0.0014965534210205078, u'target': 4, u'weight': 1, u'source': 3}, {u'lldpDelay': 0.0023109912872314453, u'delay': 0.0013430118560791016, u'target': 1, u'weight': 1, u'source': 4}, {u'lldpDelay': 0.002254962921142578, u'delay': 0.001489400863647461, u'target': 3, u'weight': 1, u'source': 4}, {u'delay': 0, u'source': 4, u'target': 4, u'weight': 0}], u'multigraph': False}, u'bw': {u'directed': True, u'graph': {}, u'nodes': [{u'id': 1}, {u'id': 2}, {u'id': 3}, {u'id': 4}, {u'id': 5}], u'links': [{u'source': 0, u'target': 0, u'weight': 0}, {u'bw_usage': 0.9593604263827729, u'target': 1, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 0, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9593604263827729, u'target': 2, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 0, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9593604263827729, u'target': 0, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 1, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'source': 1, u'target': 1, u'weight': 0}, {u'bw_usage': 1.1192538307795985, u'target': 2, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 1, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1192538307795985, u'target': 4, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 1, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9593604263827729, u'target': 0, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 2, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1192538307795985, u'target': 1, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 2, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'source': 2, u'target': 2, u'weight': 0}, {u'bw_usage': 1.1192538307795985, u'target': 3, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 2, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1192538307795985, u'target': 2, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 3, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'source': 3, u'target': 3, u'weight': 0}, {u'bw_usage': 0.9593604263827729, u'target': 4, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 3, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'bw_usage': 1.1192538307795985, u'target': 1, u'weight': 1, u'util_ratio': 0.0002238507661559197, u'source': 4, u'bandwidth': 4998.88074616922, u'cost': 1, u'congested': False}, {u'bw_usage': 0.9593604263827729, u'target': 3, u'weight': 1, u'util_ratio': 0.00019187208527655458, u'source': 4, u'bandwidth': 4999.040639573617, u'cost': 1, u'congested': False}, {u'source': 4, u'target': 4, u'weight': 0}], u'multigraph': False}}



	# print("%s, %s", net_data.graph_bw[1][2]['cost'])


def update_bandwidth(graph_bw, flow_demands, flow_paths, max_bandwidth=10000.0):
	"""
		It updates the bandwidth by reducing the bandwidth used by a flow from the links.
	:param graph_bw:
	:param flow_demands:
	:param flow_paths:
	:param max_bandwidth:
	:return:
	"""
	print
	print("flow_paths: ", flow_paths)
	print

	for f in flow_paths:
		print("PATH UPDATE: Flow: ", f)
		path = flow_paths[f]
		print("PATH ", path)
		for sw1, sw2 in zip(path[:-1], path[1:]):
			print("<PRE>flow_demands: ", flow_demands[f])
			print("<PRE>update_bandwidth(%s, %s): "% (sw1, sw2))
			print("<PRE>graph_BANDWIDTH: %s" %(graph_bw[sw1][sw2]))
			print("<PRE>update_bandwidth dic: ", graph_bw[sw1][sw2]['bandwidth'])
			if f not in sp_flow:
				bw = graph_bw[sw1][sw2]['bandwidth'] + flow_demands[f]
			else:
				bw = graph_bw[sw1][sw2]['bandwidth'] + (flow_demands[f] / 2) - 10

			print("bw: ", bw)
			if bw >= max_bandwidth: #and f not in sp_flow
				print("update_bandwidth(): NO--Change", graph_bw[sw1][sw2]['bandwidth'])
				continue
			graph_bw[sw1][sw2]['bandwidth'] = bw
			print("<POST> update_bandwidth: ", graph_bw[sw1][sw2]['bandwidth'])



def get_graph_data(network):
	print

	graph_host = json_graph.node_link_graph(network["graph"])

	graph_bw = json_graph.node_link_graph(network["bw"])

	net_data.edges = remove_self_loops(graph_host)  # set of edges in
	net_data.graph_bw = copy.deepcopy(graph_bw)  # the network graph with link_vector(cost, bandwidth, utilization Ratio, bandwidth usage, congestion status)
	flow_speed = network["flow_speed"]
	new_flows = network['new_flows']  # contains the {flows:{path:[], flowType: {S/NS}}
	net_data.flows, net_data.flow_paths = get_flows(new_flows)  # gets the flows and their paths

	net_data.flow_demands = get_flow_demands(new_flows, flow_speed)
	# print(net_data.flow_demands)
	update_bandwidth(net_data.graph_bw, net_data.flow_demands, net_data.flow_paths)
	get_nodes(graph_host.nodes())
	net_data.non_splitable_flows = net_data.flows.copy()  #it is assumed that all the flows are non_splitable

	# net_data.display_net_data()


# the graph

# print(list(graph_bw.edges(data=True)))

#
#
# # print(new_flows)
# links_delay = json_graph.node_link_graph(network['delay'])
# print
# edges = remove_self_loops(graph_host)
# print
# print(edges)
# print
# edges = graph_bw.edges(data=True)
# print(graph_bw[769][257])
# print(graph_bw.get_edge_data(769, 717))
# print("bandwidth")
# # print(edges)
# # edges = links_delay.edges(data=True)
# print("\n links delay")
# # print(edges)
#
# # print(flow_sped.edges(data=True))
# print
# print(graph_bw)
def get_nodes(nodes):
	for n in nodes:
		net_data.nodes[n]  # adds the nodes in the dictionary


def get_flow_demands(new_flows, flow_speed):
	fd = defaultdict(list)
	flow_demand = {}

	for dp in flow_speed:
		for f in flow_speed[dp]:
			demands = flow_speed[dp][f]
			fd[f] = fd[f] + demands

	print("fd amound is: ", fd)
	for f in fd:
		flow = ast.literal_eval(f)
		print("FLOW DEMAND ", flow)
		if flow in sp_flow:
			avg_speed = d
		else:
			avg_speed = np.average(fd[f])
		flow_demand[flow] = avg_speed

	return flow_demand


def get_flows(new_flows):
	"""
		It returns the flows and the paths for each flow.
	:param new_flows:
	:return:
	"""
	flows = set()
	flow_paths = {}
	print("get_flows(): ", new_flows)
	# flows_tmp = ast.literal_eval(new_flows)
	for f in new_flows:
		# print("get_flows(), path %s flows_tmp, "%(new_flows[f]['path']))

		fl = ast.literal_eval(f)
		flow_paths[fl] = new_flows[f]['path']
		# flow_path = ast.literal_eval(new)
		print fl
		# print("get_flows(), value of fl: ", fl)
		flows.add(ast.literal_eval(f))
	print("flow_paths ", flow_paths)
	return flows, flow_paths


def remove_self_loops(graph):
	loops = graph.selfloop_edges()
	graph.remove_edges_from(loops)
	edges = set(graph.edges())
	return edges


def send_msg(url, msg):
	send_msg = requests.put(url, json=msg)
	print(send_msg)

	if send_msg.status_code != 200:
		print("[PUT][Error] {}".format(send_msg.status_code))
	else:
		print("The send message was successful {}".format(send_msg.json()))
	# if send_msg.status_code != 201

def res_routing():
	paths = request_get(URL)
	print("all the paths: ", paths)

	# while True:
	# 	print("While loop...")
	# 	request_get(URL)
	# 	print("going to take a nap now...")
	# 	time.sleep(2)
	# 	print("I just woke up....")
	msg = {u'bw_usage': 1.1515393842455524, u'target': 3, u'weight': 1}

	send_msg(URL, paths)


if __name__ == '__main__':
	res_routing()
