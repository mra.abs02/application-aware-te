# TODO I haven't implemented the finding best path method here,
# XXX I created a deepcopy of the graph for sending and display to avoid the inconsistencies.

import copy
# import random
# import time

from operator import attrgetter
from ryu.base import app_manager
from ryu.base.app_manager import lookup_service_brick
from ryu.ofproto import ofproto_v1_3
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.lib import hub

from networkx.readwrite import json_graph
import mysql.connector as DataBase
from eventlet import db_pool
import numpy as np


MONITORING_PERIOD = 3.0
LINK_CAPACITY = 10000  # Since Mininet uses linux TC, OVS doesn't recognize the true capacity of the port/links
TOSHOW = True


class BandwidthMonitor(app_manager.RyuApp):
	OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

	def __init__(self, *args, **kwargs):
		super(BandwidthMonitor, self).__init__(*args, **kwargs)
		self.name = "BandwidthMonitor"
		self.topology = lookup_service_brick('TopologyDiscovery')

		self.datapaths = {}  # {dpid: dpid_object_address} Stores the path objects
		self.port_desc = {}  # {dpid: {port_no: port_features} it stores the port_features = (config, state, curr_speed) Alias: fort_state
		self.port_stats = {}  # {(dpid, port_no): [(tx_byte, rx_bytes, rx_errors, duration_sec, duration_nsec), ...]}
		self.port_speed = {}  # {(dpid, port_no): [speed1, speed2, ...]}
		self.stats = {}  # {'flow':{dpid: stat_body}, 'port': {dpid: stat_body}}
		self.free_bandwidth = {}  # {dpid: {port_no: free_bw}}
		self.flow_stats = {}  # {dpid: {(priority, ip_src, ip_dst): [(packet_count, byte_count, duration_sec, duration_nsec),]}}
		self.flow_speed = {}  # {dpid: {(priority, ip_src, ip_dst): [speed1, speed2,...]}}
		self.json_flow_speed_no_priority = {}  # {dpid: {(ip_src, ip_dst): [speed1, speed2,...]}}
		self.json_flow_speed = {}  # {dpid: {(ip_src, ip_dst): [speed1, speed2,...]}}

		self.congested_links = set()  # set of congested links TODO: just in case I need it.

		self.graph = None
		self.graph_pre = None	# previous period graph data

		self.save_bandwidth_thread = hub.spawn(self._save_bw)
		self.bandwidth_monitoring_thread = hub.spawn(self._monitor)
		self.time_id = 0

		self.db_pool = db_pool.ConnectionPool(DataBase, host="localhost",
			user="mra",
			passwd="mra147",
			database="ryu"
		)
		# self.mydb = mysql.connector.connect(
		# 	host="localhost",
		# 	user="mra",
		# 	passwd="mra147",
		# 	database="ryu"
		# )
		mydb = self.db_pool.get()
		try:
			mycursor = mydb.cursor()
			# creating the table for throughput data
			mycursor.execute("DROP TABLE IF EXISTS tp")
			mycursor.execute("CREATE TABLE tp (id int NOT NULL AUTO_INCREMENT PRIMARY KEY,  "
								  "dpid int(11) NOT NULL,  timeid varchar(10) NOT NULL,"
								  "address varchar(100) NOT NULL, speed double NOT NULL )")
			# creating the table for link utilization and utilization ratio
			mycursor.execute("DROP TABLE IF EXISTS util")
			mycursor.execute("CREATE TABLE util (id int NOT NULL AUTO_INCREMENT PRIMARY KEY,  "
								  "edge varchar(100) NOT NULL, timeid varchar(10) NOT NULL,"
								  "util double NOT NULL, util_ratio double NOT NULL)")
		finally:
			self.db_pool.put(mydb)

	def _save_bw(self):
		"""
			This thread save the bandwidth values into the topology graph every MONITORING_PERIOD.
		"""
		hub.sleep(11)
		while True:
			self.graph = self.store_bw_in_topology_graph(self.free_bandwidth)
			hub.sleep(MONITORING_PERIOD)
			# self.display_graph_data()
			# if self.graph is not None:
			# 	self.graph_pre = self.graph.copy()

	def _monitor(self):
		"""
			Main entry method of monitoring traffic.
		"""
		hub.sleep(11)
		while True:
			self.stats['flow'] = {}
			self.stats['port'] = {}
			for dp in self.datapaths.values():
				self.port_desc.setdefault(dp.id, {})
				self._send_stats_request(dp)
				# Refresh data.
				# self.capabilities = None
				# self.best_paths = None
			hub.sleep(MONITORING_PERIOD)
			if self.stats['flow'] or self.stats['port']:
				self.display_stat('flow')
				self.display_stat('port')
				hub.sleep(1)
			self.time_id += 1

	def get_graph_object_with_bw(self, mode='object'):
		self.logger.debug("[INFO][get_graph_bw] is called in {} mode.".format(mode))
		if mode == "json":
			json_topo = json_graph.node_link_data(self.graph_pre)
			# TODO get the json file for the self.flow_stat{}
			return json_topo
		else:
			return self.graph

	def get_flow_speed(self):
		"""
			Returns the flow speed (with and without priority).
		"""
		self.logger.info("get_flow_speed api is called....")
		return self.json_flow_speed, self.json_flow_speed_no_priority


	@set_ev_cls(ofp_event.EventOFPStateChange, [MAIN_DISPATCHER, DEAD_DISPATCHER])
	def _state_change_handler(self, ev):
		"""
			This module register/deregister the datapaths.
		"""
		datapath = ev.datapath
		if ev.state == MAIN_DISPATCHER:
			if not datapath.id in self.datapaths:
				self.logger.info('[DEBUG][BandwidthMon] Registering datapath %s', datapath.id)
				self.datapaths[datapath.id] = datapath

				# self._send_stats_request(datapath)
		elif ev.state == DEAD_DISPATCHER:
			self.logger.info('[DEBUG][BandwidthMon] Deregistering datapath %s', datapath.id)
			if datapath.id in self.datapaths:
				del self.datapaths[datapath.id]

	def _send_stats_request(self, datapath):

		"""
		 	It sends the static request for ports and flows to the datapath
		"""
		ofproto = datapath.ofproto
		parser = datapath.ofproto_parser

		# port description request
		req = parser.OFPPortDescStatsRequest(datapath, 0)  # the second arg is to indicate if more request is coming
		# i.e., it is OFPMPF_REQ_MORE or multipart message.
		datapath.send_msg(req)
		# port statistics request
		req = parser.OFPPortStatsRequest(datapath, 0, ofproto.OFPP_ANY)
		datapath.send_msg(req)
		# flow statistics
		req = parser.OFPFlowStatsRequest(datapath)
		datapath.send_msg(req)
		self.logger.debug("[DEBUG][BandwidthMon][req_stat()] send request to datapath %016x", datapath.id)

	@set_ev_cls(ofp_event.EventOFPPortDescStatsReply, MAIN_DISPATCHER)
	def _port_desc_stats_reply_handler(self, ev):
		"""
			It handles PortDescStatsReply, and stores the current port speed, its configuration i.e., up, down, etc, and
			the port (link) states. These information is stored in the self.port_desc dictionary.
		"""

		msg = ev.msg
		datapath = msg.datapath
		ofproto = msg.datapath.ofproto

		port_config = {
			ofproto.OFPPC_PORT_DOWN: "Down",
			ofproto.OFPPC_NO_RECV: "No_Recv",
			ofproto.OFPPC_NO_FWD: "No_Forward",
			ofproto.OFPPC_NO_PACKET_IN: "No_Packet-in"
		}
		port_state = {
			ofproto.OFPPS_LINK_DOWN: "Down",
			ofproto.OFPPS_BLOCKED: "Blocked",
			ofproto.OFPPS_LIVE: "Live"
		}

		# ports = []
		for p in msg.body:
			# ports.append(
			# 	'port_no=%d hw_addr=%s name=%s config=0x%08x '
			# 	'state=0x%08x curr=0x%08x advertised=0x%08x '
			# 	'supported=0x%08x peer=0x%08x curr_speed=%d '
			# 	'max_speed=%d' %
			# 	(p.port_no, p.hw_addr,
			# 	 p.name, p.config,
			# 	 p.state, p.curr, p.advertised,
			# 	 p.supported, p.peer, p.curr_speed,
			# 	 p.max_speed)
			# )

			if p.config in port_config:
				config = port_config[p.config]
			else:
				config = "UP"

			if p.state in port_state:
				state = port_state[p.state]
			else:
				state = "UP"

			port_feature = (config, state, p.curr_speed)
			self.port_desc[datapath.id][p.port_no] = port_feature

		self.logger.debug("[DEBUG][Bandwidth][port_desc()] self.port_des = %s", self.port_desc)

	@set_ev_cls(ofp_event.EventOFPPortStatsReply, MAIN_DISPATCHER)
	def _port_stats_reply_handler(self, ev):
		stat_body = ev.msg.body
		dpid = ev.msg.datapath.id

		self.stats['port'][dpid] = stat_body
		self.free_bandwidth.setdefault(dpid, {})

		# self.logger.info("[DEBUG][Bandwidth][port_stat()] datapath %s replied", stat_body)
		for stat in sorted(stat_body, key=attrgetter('port_no')):
			port_no = stat.port_no
			if port_no != ofproto_v1_3.OFPP_LOCAL:
				key = (dpid, port_no)
				value = (
					stat.tx_bytes, stat.rx_bytes, stat.rx_errors,
					stat.duration_sec, stat.duration_nsec
				)
				self._save_stats(self.port_stats, key, value, 5)

				pre = 0
				period = MONITORING_PERIOD
				tmp = self.port_stats[key]
				if len(tmp) > 1:
					# currently I pass both the rx and tx bytes
					pre = tmp[-2][0] + tmp[-2][1]  # TODO check whether both the rx and tx are needed for bw/speed
					period = self._get_period(tmp[-1][3], tmp[-1][4], tmp[-2][3], tmp[-2][4])
				speed = self._get_speed(self.port_stats[key][-1][0] + self.port_stats[key][-1][1], pre, period)
				self._save_stats(self.port_speed, key, speed, 5)
				self._save_freebandwidth(dpid, port_no, speed)

	@set_ev_cls(ofp_event.EventOFPPortStatus, MAIN_DISPATCHER)
	def _port_status_handler(self, ev):
		"""
			Handle the port status changed event.
		"""
		msg = ev.msg
		ofproto = msg.datapath.ofproto
		reason = msg.reason
		dpid = msg.datapath.id
		port_no = msg.desc.port_no

		reason_dict = {
			ofproto.OFPPR_ADD: "Added",
			ofproto.OFPPR_DELETE: "Deleted",
			ofproto.OFPPR_MODIFY: "Modified",
		}

		if reason in reason_dict:
			print "switch%d: port %s %s" % (dpid, reason_dict[reason], port_no)
		else:
			print "switch%d: Illegal port state %s %s" % (dpid, port_no, reason)

	def save_throughput_dp(self, dpid, src, dst, speed):
		# insert the speed into the dp.
		# random.seed(time.time())
		# prob = random.uniform(0, 1)
		# if speed > LINK_CAPACITY:
		# 	frac = random.randint(50, 180)
		# 	if prob > 0.5:  # 50% chance that the a fraction is added to actual speed or removed from the speed
		# 		speed = LINK_CAPACITY + frac
		# 	else:
		# 		speed = LINK_CAPACITY - frac
		# 	print("THE SPEED IS > LINK CAPACITY...: The new speed is, ", speed)

		addr = str(src) + "-" + str(dst)
		sql = "INSERT INTO tp (dpid, timeid, address, speed) VALUES (%s, %s, %s, %s)"
		val = [(dpid, str(self.time_id), addr, speed)]

		mydb = self.db_pool.get()
		try:
			mycursor = mydb.cursor()
			mycursor.executemany(sql, val)
			mydb.commit()
		finally:
			self.db_pool.put(mydb)



	@set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
	def _flow_stats_reply_handler(self, ev):
		stat_body = ev.msg.body
		dpid = ev.msg.datapath.id

		self.flow_stats.setdefault(dpid, {})
		self.flow_speed.setdefault(dpid, {})
		self.json_flow_speed_no_priority.setdefault(str(dpid), {})
		self.json_flow_speed.setdefault(str(dpid), {})

		self.stats['flow'][dpid] = stat_body
		self.logger.debug("[DEBUG][Bandwidth][flow_stat()] datapath %s replied %s", ev.msg.datapath.id, stat_body)

		for stat in sorted([flow for flow in stat_body if (
				(flow.priority not in [0, 65535]) and (flow.match.get('ipv4_src')) and (flow.match.get('ipv4_dst')))],
						   key=lambda flow: (flow.priority, flow.match.get('ipv4_src'), flow.match.get('ipv4_dst'))):

			key = (stat.priority, stat.match.get('ipv4_src'), stat.match.get('ipv4_dst'))
			value = (stat.packet_count, stat.byte_count, stat.duration_sec, stat.duration_nsec)
			self._save_stats(self.flow_stats[dpid], key, value, 5)

			# Get flow's speed and Save it.
			pre = 0
			period = MONITORING_PERIOD
			tmp = self.flow_stats[dpid][key]
			if len(tmp) > 1:
				pre = tmp[-2][1]
				period = self._get_period(tmp[-1][2], tmp[-1][3], tmp[-2][2], tmp[-2][3])
			speed = self._get_speed(self.flow_stats[dpid][key][-1][1], pre, period)
			self._save_stats(self.flow_speed[dpid], key, speed, 5)

			# flow speeds with keys converted to string for json with priority use and convert
			self._save_stats(self.json_flow_speed[str(dpid)], str(key), speed, 5)

			# flow speeds with keys converted to string for json without priority use and convert the speed to kbps
			key_no_priority = str((stat.match.get('ipv4_src'), stat.match.get('ipv4_dst')))
			speed = self.conv_to_kbps(speed)
			if speed > 0.0:
				self._save_stats(self.json_flow_speed_no_priority[str(dpid)], key_no_priority, speed, 5)

				# insert the speed into the dp.
				tmp_speed = self.json_flow_speed_no_priority[str(dpid)][key_no_priority]
				if len(tmp_speed) > 1:
					speed_list = tmp_speed[-2:]
				else:
					speed_list = tmp_speed
				avg_speed  = float(np.average(tmp_speed))
				# avg_speed  = self.json_flow_speed_no_priority[str(dpid)][key_no_priority][-1]
				self.save_throughput_dp(dpid, stat.match.get('ipv4_src'), stat.match.get('ipv4_dst'), avg_speed)
				# addr = str() + "-" + str()
				# sql = "INSERT INTO tp (dpid, timeid, address, speed) VALUES (%s, %s, %s, %s)"
				# val = [(dpid, str(self.time_id), addr, avg_speed)]
				# self.mycursor.executemany(sql, val)
				# self.mydb.commit()

	def conv_to_kbps(self, speed):
		"""
			Convert the speed from bps to kbps
		:param speed: speed in bps
		:return: speed in Kbps
		"""
		return speed * 8 / 1000.0

	def _save_stats(self, stats_dict, key, value, no_of_entries=5):
		"""
			It stores the latest statistics.
		:param stats_dict: the statistic dictionary e.g., port_stat, flow_stat, etc.
		:param no_of_entries: no of entries to store for each key in the stat_dict.
		"""
		if key not in stats_dict:
			stats_dict[key] = []
		stats_dict[key].append(value)
		if len(stats_dict[key]) > no_of_entries:
			stats_dict[key].pop(0)
		self.logger.debug("[DEBUG][Bandwidth_mon][save_stats()] stat for dic %s are %s.", stats_dict, stats_dict)

	def _get_period(self, sec_dur_latest, nano_sec_latest, sec_dur_pre, nano_sec_pre):
		"""
			Return the period.
		:param sec_dur_latest: The latest duration in seconds
		:param nano_sec_latest: The latest duration in nano seconds
		:param sec_dur_pre: The previous duration in seconds
		:param nano_sec_pre: The previous duration in nano seconds
		"""
		return self._get_time(sec_dur_latest, nano_sec_latest) - self._get_time(sec_dur_pre, nano_sec_pre)

	def _get_time(self, sec, nano_sec):
		"""
			Calculate Total duration in seconds.
		"""
		return sec + nano_sec / 1000000000.0

	def _get_speed(self, current, pre, period):
		"""
			Calculate the speed for a period.
		:param current: current tx + rx bytes
		:param pre: previous transmitted bytes
		:return: the speed for the given period.
		"""
		if period:
			return (current - pre) / period
		else:
			return 0

	def _save_freebandwidth(self, dpid, port_no, speed):

		port_capacity = LINK_CAPACITY  # Since Mininet uses linux TC OVS doesn't recognize the true capacity of the port
		port_feature = self.port_desc.get(dpid).get(port_no)
		if port_feature:
			free_bw = self._get_free_bw(port_capacity, speed)
			self.free_bandwidth[dpid].setdefault(port_no, None)
			self.free_bandwidth[dpid][port_no] = free_bw
			self.logger.debug("[Debug][Bandwidth_mon][save_freebw()] self.free_bandwidth %s .", self.free_bandwidth)
		else:
			self.logger.info("[Warning][Bandwidth_mon][save_freebw()] Port is not recognized.")

	def _get_free_bw(self, capacity, speed):
		"""
			Calculates the free bandwidth on a port in Kbit/s
		"""
		return max(capacity - speed * 8 / 1000.0, 0)

	def _calculate_cost_utilization(self, free_bw, link_cap):
		"""
		    it calculates the bandwidth utilization, link cost and utilization ratio, and if the link is congested or not.
		:param free_bw: the free_bw of this link.
		:param link_cap: The link capacity.
		:return: link_cost, utilization_ratio, congested, bandwidth_usage
		"""
		bandwidth_usage = abs(link_cap - free_bw)
		utilization_ratio = bandwidth_usage / float(link_cap)
		link_cost = 1
		if utilization_ratio >= 0 and utilization_ratio < (1.0/3.0):
			link_cost = 1
		elif utilization_ratio >= (1.0/3.0) and utilization_ratio < (2.0/3.0):
			link_cost = 3
		elif utilization_ratio >= (2.0/3.0) and utilization_ratio < (9.0/10.0):
			link_cost = 10
		elif utilization_ratio >= (9.0/10.0) and utilization_ratio < 1.0:
			link_cost = 70
		elif utilization_ratio >= 1.0 and utilization_ratio < (11.0/10.0):
			link_cost = 500
		elif utilization_ratio >= (11.0/10.0) and utilization_ratio < float('inf'):
			link_cost = 5000

		if utilization_ratio >= 1:
			congested = True
		else:
			congested = False

		return link_cost, utilization_ratio, congested, bandwidth_usage


	def save_utilization_db(self, src, dst, util, util_ratio):
		# insert the utilization into the dp.
		edge = str(src) + "-" + str(dst)
		sql = "INSERT INTO util (edge, timeid, util, util_ratio) VALUES (%s, %s, %s, %s)"
		val = [(edge, str(self.time_id), util, util_ratio)]

		mydb = self.db_pool.get()
		try:
			mycursor = mydb.cursor()
			mycursor.executemany(sql, val)
			mydb.commit()
		finally:
			self.db_pool.put(mydb)


	def store_bw_in_topology_graph(self, bw_dict):
		"""
			Stores the bandwidth values into the network topology graph.
		:param bw_dict:
		:return:
		"""
		if self.topology is None:
			self.logger.info("[Error][BandwidthMon][save_bw] topology is None")

		try:
			# graph = self.topology.get_graph_object()
			graph = self.topology.graph

			link_to_port = self.topology.link_to_port

			for link in link_to_port:
				(src_dpid, dst_dpid) = link
				(src_port, dst_port) = link_to_port[link]
				if src_dpid in bw_dict and dst_dpid in bw_dict:
					bw_src = bw_dict[src_dpid][src_port]
					bw_dst = bw_dict[dst_dpid][dst_port]
					bandwidth = min(bw_src, bw_dst)
					# Add key:value pair of bandwidth into graph.
					link_cost, utilization_ratio, congested, bandwidth_usage = self._calculate_cost_utilization(bandwidth, LINK_CAPACITY)
					# self.logger.info("[DEBUG][Link_info] link (%s, %s) bw: %s, cost: %s, uratio: %s, cong: %s, bu: %s"
					# 				 %(src_dpid, dst_dpid, bandwidth, link_cost, utilization_ratio, congested, bandwidth_usage))

					if graph.has_edge(src_dpid, dst_dpid):
						graph[src_dpid][dst_dpid]['bandwidth'] = bandwidth
						graph[src_dpid][dst_dpid]['cost'] = link_cost
						graph[src_dpid][dst_dpid]['util_ratio'] = utilization_ratio
						graph[src_dpid][dst_dpid]['congested'] = congested
						graph[src_dpid][dst_dpid]['bw_usage'] = bandwidth_usage

						# saving the link utilization in the database
						self.save_utilization_db(src_dpid, dst_dpid, bandwidth_usage, utilization_ratio)

					else:
						graph.add_edge(src_dpid, dst_dpid)
						graph[src_dpid][dst_dpid]['bandwidth'] = bandwidth
						graph[src_dpid][dst_dpid]['cost'] = link_cost
						graph[src_dpid][dst_dpid]['util_ratio'] = utilization_ratio
						graph[src_dpid][dst_dpid]['congested'] = congested
						graph[src_dpid][dst_dpid]['bw_usage'] = bandwidth_usage
						# saving the link utilization in the database
						self.save_utilization_db(src_dpid, dst_dpid, bandwidth_usage, utilization_ratio)

					if congested:
						self.congested_links.add((src_dpid, dst_dpid))

				else:
					# TODO if there was some problem I may need to add cost, uratio, cost in this block too. But
					# I believe this block usually runs in the initial phase of the controller.
					if graph.has_edge(src_dpid, dst_dpid):
						graph[src_dpid][dst_dpid]['bandwidth'] = 0

					else:
						graph.add_edge(src_dpid, dst_dpid)
						graph[src_dpid][dst_dpid]['bandwidth'] = 0

			self.logger.debug("[BandwidthMon][save_bw] Saved bandwidth in the graph.")
			self.graph_pre = copy.deepcopy(graph)
			# self.logger.info("[DEBUG][save_bw_in_G] the graph data => %s" % (self.graph_pre.edges(data=True)))
			# self.logger.info("The set of congested links: ", self.congested_links)
			return graph
		except:
			self.logger.info("[Error][BandwidthMon][store_bw_in_topology_graph()] Could not store the bandwidth.")
			if self.topology is None:
				self.topology = lookup_service_brick('TopologyDiscovery')
			return self.topology.graph

	def display_stat(self, _type):
		"""
			Show statistics information according to data type.
			_type: 'port' / 'flow'
		"""
		if TOSHOW is False:
			return

		bodys = self.stats[_type]
		if _type == 'flow':
			print('\ndatapath  '
				  'priority        ip_src        ip_dst  '
				  '  packets        bytes  flow-speed(Kb/s)')
			print('--------  '
				  '--------  ------------  ------------  '
				  '---------  -----------  ----------------')
			for dpid in sorted(bodys.keys()):

				for stat in sorted([flow for flow in bodys[dpid] if (
						(flow.priority not in [0, 65535]) and (flow.match.get('ipv4_src')) and (
						flow.match.get('ipv4_dst')))],
								   key=lambda flow: (
										   flow.priority, flow.match.get('ipv4_src'), flow.match.get('ipv4_dst'))):
					print('%8d  %8s  %12s  %12s  %9d  %11d  %16.1f' % (
						dpid,
						stat.priority, stat.match.get('ipv4_src'), stat.match.get('ipv4_dst'),
						stat.packet_count, stat.byte_count,
						abs(self.flow_speed[dpid][
								(stat.priority, stat.match.get('ipv4_src'), stat.match.get('ipv4_dst'))][
								-1]) * 8 / 1000.0))
			print

			self.logger.info("[DEBUG][bw_mon] self.flow_speed %s", self.json_flow_speed)
			self.logger.info("[DEBUG][bw_mon] self.flow_speed_no_prio %s", self.json_flow_speed_no_priority)

		if _type == 'port':
			print('\ndatapath  port '
				  '   rx-pkts     rx-bytes ''   tx-pkts     tx-bytes '
				  ' port-bw(Kb/s)  port-speed(b/s)  port-freebw(Kb/s) '
				  ' port-state  link-state')
			print('--------  ----  '
				  '---------  -----------  ''---------  -----------  '
				  '-------------  ---------------  -----------------  '
				  '----------  ----------')
			_format = '%8d  %4x  %9d  %11d  %9d  %11d  %13d  %15.1f  %17.1f  %10s  %10s'
			for dpid in sorted(bodys.keys()):
				for stat in sorted(bodys[dpid], key=attrgetter('port_no')):
					if stat.port_no != ofproto_v1_3.OFPP_LOCAL:
						print(_format % (
							dpid, stat.port_no,
							stat.rx_packets, stat.rx_bytes,
							stat.tx_packets, stat.tx_bytes,
							10000,
							abs(self.port_speed[(dpid, stat.port_no)][-1] * 8),
							self.free_bandwidth[dpid][stat.port_no],
							self.port_desc[dpid][stat.port_no][0],
							self.port_desc[dpid][stat.port_no][1]))
			print

	def display_graph_data(self):
		print "---------------------Topo Link---------------------"
		print '%10s' % ("switch"),
		for i in self.graph.nodes():
			print '%10d' % i,
		print ""
		if self.graph_pre is not None:
			for u, v, d in self.graph_pre.edges(data='bandwidth'):
				print 'edge (%s, %s)[%s]'%(u, v, d),
				print ""
		# if self.graph_pre is not None:
		# 	edges = [self.graph_pre.edges(data=True)]
		# 	print("[Debug][bw_edge] ", edges)