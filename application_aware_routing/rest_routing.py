# TODO check if the flows and bandwidth are in kbps or mbps. line 272 in bw_mon has the for conversion.
# flow_speed with no priority, the speed is already converted.

import requests
from networkx.readwrite import json_graph
import time
import networkx as nx

URL = "http://127.0.0.1:8080/no/mac/data"

def request_get(url):
	try:
		req_get = requests.get(url)

		if req_get.status_code != 200:
			print("GET error: {}".format(req_get.status_code))
			# exit()

		network = req_get.json()
		# XXX ------------ This code gets the bw and graph information in the dictionary format {'graph': {}, 'bw': {}}
		# however I believe the bw app store the data in the original graph, so the information is identical in both graph and
		# bw dictionaries.
		# print("\n\n")
		# print("---------------network info ---------------------\n")
		print(network)
		print("\n\n")
		print("The Graph is: ", network['graph'])
		# graph = json_graph.node_link_graph(network["graph"])
		print("The Graph+bandwidth: ", network["bw"])
		graph_bw = json_graph.node_link_graph(network["bw"])
		print("\n the flow_speed : \n", network["flow_speed"])
		print("\n new_flows: \n", network['new_flows'])
		print("\n network_delay: \n", network['delay'])
		# print(graph, graph_bw)
	except:
		print("Couldn't connect to the server... Sleeping now")


# ------------------------- The End ----------------------------------


# XXX ---------- This part convert the network into loopless graph
# edges = list(topo_graph.edges())
# G = nx.DiGraph()
# loop_less = G.remove_edges_from(topo_graph.selfloop_edges())
# ebunch = topo_graph.selfloop_edges()
# topo_graph.remove_edges_from(ebunch)
# g_edges=list(topo_graph.edges())
# print("list of edges %s and ebunch %s, loop-less edges %s"%(edges, ebunch, g_edges))
# ----------------- End -------------------------


def send_msg(url, msg):
	send_msg = requests.put(url, json=msg)
	print(send_msg)

	if send_msg.status_code != 200:
		print("[PUT][Error] {}".format(send_msg.status_code))
	else:
		print("The send message was successful {}".format(send_msg.json()))

#	if send_msg.status_code != 201

def res_routing():
	while True:
		print("While loop...")
		request_get(URL)
		print("going to take a nap now...")
		time.sleep(2)
		print("I just woke up....")
		# send_msg(url, msg)





if __name__ == '__main__':
	res_routing()

