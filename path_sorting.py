from collections import defaultdict
# TODO how to sort the edges such that (i, j)(j, e), i.e., for if e[1] == e[0] they should be next to each other
# if the flow vlues are different there is no problem with the following code, however if the vlues are same then we have some problem.
# Let me try the code where it findes the edges coming in and out of edge in combination with the following code. Maybe I can get some where.
# i.e, if I create the in and out edges using the given paths by CPLEX then I can find out which node is the group_Flow and which one is just single paths.
 #  edges = [[1,'s'],[2,1],[4,2],[5,4],[3,1],[4,3],[2,4],[5,2],['t',5]]
edges = {
     ('s', 1): 1,
     (1, 3): 3,
     (5, 't'): 1,
     (1, 2): 7,
     (3, 4): 3,
     (2, 4): 7,
     (4, 5): 7,
     (4, 2): 3,
     (2, 5): 3,
}
#edges = {
     #('s', 1): 1,
     #(1, 3): 3,
     #(5, 't'): 1,
     #(1, 2): 3,
     #(3, 4): 3,
     #(2, 4): 3,
     #(4, 5): 3,
     #(4, 2): 3,
     #(2, 5): 3,
#}
flows_edges = defaultdict(list)

for key, value in edges.items():
    print(key, value)
    flows_edges[value].append(key)
    
print("flows_edges, ", flows_edges)

flow_path_nodes = defaultdict(list)

for flow, edges in flows_edges.items():
    for e in edges:
        i = e[0]
        j = e[1]
        print((i,j))
        if i not in flow_path_nodes[flow]:
            flow_path_nodes[flow].append(i)
        if j not in flow_path_nodes[flow]:
            flow_path_nodes[flow].append(j)

print(flow_path_nodes)        


#def first_elem(elem):
        #return elem[1]

#for flow, path in flows_edges.items():
    #if type(path[0][0]) == str:
        #continue
    #sorted_edges = sorted(path[1:], key=first_elem)
    #flows_edges[flow] = path[sorted_edges

#print(flows_edges)
